/* jshint node: true */
/* globals JiraCachedDB: false */
/* globals google: false */
/* globals document: false */
'use strict';
const SERVER = "https://innocells.atlassian.net";

//define("_ujg_gft_dashboard_bsdroad_control_panel", [], function() {let fn = function(API) { _ujg_gft_dashboard_bsdroad_control_panel(API); }; return fn;});
const MMS2DAYS = 86400000; // Conversion factor from milliseconds to days.
const RELEASE_DAY = 12; // Number of month day used to convert release conventional names XX.XX to a date object.
const JQL_LINK = "https://innocells.atlassian.net/issues/?jql=";
let _ujgAPI; // Global acces to Universal Jira Gadget API.
let _ujg_gft_dashboard_bsdroad_main_query = {}; // Global variable to store main JQL query result.

/** Shortcut for document.getElementById function
 * @param {string} id 
 * @returns Object with the required id.
 */
function $$ (id) {return document.getElementById(id);}

/** Format a decimal number to its percentatge equivalent.
 * @param {float} value Float number to format
 * @returns String with the number formatted.
 */
const percent = (value) => {
    /* jshint -W117 */ // ignore warning for the use of "Intl"
    return Intl.NumberFormat("en-US", {
        style: "percent",
        minimumFractionDigits: 1,
        maximumFractionDigits: 2
    }).format(value);
};

/** Generic function to show errors.
 * @param {string} data Response JSON from request
 */
function _ujg_gft_generic_onError(data){    
    let error_message = JSON.parse(data.response).errorMessages;
    console.log(error_message);
    _ujg_gft_data_load_statusText(`<span style='color:red'>Error cargando los datos: </span>${error_message}`);
    _ujgAPI.resize();
}

/** Return issues array based on jql query 
 * @param {string} jql Query to execute in Jira
 * @param {function} forEachElementFunctionCall function to call for each recovered item. 
 * @returns Issues array.
 */
async function _ujg_gft_data_load_jira_dataset(jql, forEachElementFunctionCall=null){
    // Query cache.
    let cache = new JiraCachedDB();
    await cache.open();

    // Download candidate issues.
    _ujg_gft_data_load_statusText("Buscando épicas.");
    let JQLSearch = await cache.JQLQuery(jql, _ujg_gft_data_load_progressbar, ['Issue ']);
    JQLSearch.jql = jql;
    JQLSearch.lookup = {};
     
    // Download issues and changelogs.
    _ujg_gft_data_load_statusText("Descargando épicas.");
    let progress=0;
    for (let i=0; i<JQLSearch.issues.length; i++) {
        let foundIssue = JQLSearch.issues[i];
        JQLSearch.lookup[foundIssue.key] = i;
        cache.issue(foundIssue, "changelog", forEachElementFunctionCall).then( 
            /* jshint -W083 */ // ignore warning for the use of "Intl"
            (response) => {
                JQLSearch.issues[i] = response;
                _ujg_gft_data_load_progressbar(progress++, JQLSearch.issues.length, "epicas");
            }
        );
    }

    // Wait all elements to arrive.
    await cache.flush();

    // Remove status and download bar. Enable toolbar.
    $$('_ujg_gft_status_div').style.display="none";
    $$('_ujg_gft_dashboard_div').style.display="initial";

    // Return resultant values.
    return JQLSearch;
}

/** Create a new JQL query injecting a filter (subquery) into the main query.
* @param {*} jql main query
* @param {*} filter subquery to inject
* @returns JQL string with both filters on AND logic.
*/
function _ujg_gft_data_load_inject_jql_filter(jql, filter){
    // If no filter is set or is empty, do nothing.
    if (!filter || filter == "") {
        return jql;
    }

    // Inject filter as AND condition between previous conditions and ORDER BY clause if exits.
    let parts = jql.split(" ORDER BY ");
    parts[0] = `(${parts[0]}) AND (${filter})`;
    return parts.join(" ORDER BY ");
}

/** Extract only issues from the dataset accordingly to indicated JQL.
* @param {*} dataset primary dataset.
* @param {*} jql query of issues to filter.
* @param {*} forEachElementFunctionCall Optional. Call a function for each element.
* @returns Filtered dataset.
*/
async function _ujg_gft_data_load_filter_jira_dataset(dataset, jql, forEachElementFunctionCall=null){
    // Local accés to the caché.
    let cache = new JiraCachedDB();
    await cache.open();

    // Prepare the refiltered query and get the list of issues.
    let jql_filtered = _ujg_gft_data_load_inject_jql_filter(dataset.jql,jql);
    let JQLSearch = await cache.JQLQuery(jql_filtered, _ujg_gft_data_load_progressbar, ['Issue ']);

    // Lookup key to easily search keys in the array.
    JQLSearch.lookup = {};

    // Store the JQL as atribute of the returned object to be available externally.
    JQLSearch.jql = jql_filtered;
        
    // Copy found elements in the filter from the dataset to the response.
    for (let i=0; i<JQLSearch.issues.length; i++) {
        let foundIssue = JQLSearch.issues[i];
        JQLSearch.lookup[foundIssue.key] = i;
        JQLSearch.issues[i] = dataset.issues[dataset.lookup[foundIssue.key]];
    }

    // Return resultant values.
    return JQLSearch;
}

/** Update status area with the text in param.
 * @param {string} text Text to show.
 */
function _ujg_gft_data_load_statusText(text){
    $$('_ujg_gft_status_line').innerHTML = text;
}

/** Show a status message and a progress bar acording to the current process status.
 * @param {integer} current Number of items processed
 * @param {integer} total  Total items to process
 * @param {string} label Type of item to status text.
 */
function _ujg_gft_data_load_progressbar(current, total, label){
    let percent = Math.floor(current * 100 / total);
    _ujg_gft_data_load_statusText(`<div style='color:orange'>Extracción de ${label} al ${percent}% (${current} de ${total})</div>`);
    $$('_ujg_gft_progress_bar').style.width = `${percent}%`;
}

/** Convert fixVersion conventional names to milliseconds unix time.
 * @param {String} fixVersion to convert
 * @returns Milliseconds get from data object.
 */
 function _ujg_gft_math_fixversion_to_time(fixVersion){
    fixVersion = fixVersion.split(",")[0].trim().toUpperCase();
    fixVersion = fixVersion.replace(".XX",".12");
    fixVersion = fixVersion.replace(".X",".12");
    let date = fixVersion.split(".");
    return (new Date(`20${date[0]}-${date[1]}-${RELEASE_DAY}`)).getTime();
}

/** Calculate time to market based on state changes.
 * @param {object} changelog Issue changelog object prepared by _ujg_gft_statistics_analyze_changelog function.
 * @returns number of days between start and end states.
 */
function _ujg_gft_math_calculate_time2market(changelog){
    let response = 0;

    let state_changes = changelog.state_changes;
    let latest_version = "";
    let latest_version_date ;

    latest_version = changelog.versions.latest;
    if (latest_version){
        latest_version_date = new Date(latest_version);
    }

    // Only calculate time2market if the issue had needed states ([KongoKO] or [Analisis]) and ([Producción] or version)
    if ((state_changes[10188] || state_changes[10117]) && ( state_changes[10118] || latest_version_date)){
        let start_date = state_changes[10188] ? state_changes[10188].firstPass : state_changes[10117].firstPass;
        let end_date = state_changes[10118] ? state_changes[10118].lastPass : latest_version_date;
        response = parseInt(end_date- start_date) / MMS2DAYS ; 
    }
    return response;
}

/** Generate a dictionary with all states (key) the issue passed and relevant information when it happens.
 * @param {object} issue to analyze its changelog.
 * @returns dictionary with states and its information.
 */
 function _ujg_gft_statistics_analyze_changelog(issue){
    // local variable to work.
    let last_pass_to = {};
    let transitions = [];
    let versions = [];
    versions.changelog = [];
    let replanification = false;

    // Initialize generic information.
    last_pass_to[0] = {};
    last_pass_to[0].name = "created";
    last_pass_to[0].firstPass = new Date(issue.fields.created);
    last_pass_to[0].lastPass = last_pass_to[0].firstPass;
    versions.replanifications = 0;
    versions.deletes = 0;
    //versions.latest ;
    //versions.first;
    
    // Store last pass for each status to an indexed dictionary.
    for (let i=0; i<issue.changelog.values.length; i++){
        let changes = issue.changelog.values[i];
        for (let j=0; j<changes.items.length; j++){
            let change = changes.items[j];
            switch (change.fieldId){
                case "status":
                    // Create transition structure.
                    let transition = {};
                    transition.from = {};
                    transition.to = {};
                    transition.id = `${change.from}>${change.to}`;
                    transition.from.id = change.from;
                    transition.from.string = change.fromString;
                    transition.to.id = change.to;
                    transition.to.string = change.toString;
                    transition.start = new Date(changes.created);
                    transitions.push(transition);
                    if (transitions.length > 1){
                        let pt = transitions.length-2; // Previous Transition.
                        transitions[pt].end = new Date(changes.created);
                        transitions[pt].duration = (transitions[pt].end - transitions[pt].start) / MMS2DAYS ;
                    }

                    // Create last_pass structure.
                    if ( !last_pass_to[change.to] ){
                        last_pass_to[change.to] = {};
                        last_pass_to[change.to].name = change.toString;
                    }
                    last_pass_to[change.to].lastPass = new Date(changes.created);
                    if (!last_pass_to[change.to].firstPass){
                        last_pass_to[change.to].firstPass = new Date(changes.created);
                    }

                    // Activate replanifications count if status was on R4b
                    if (change.toString == "KonGo R4B"){
                        replanification = true;
                    }
                    break;
                
                case "fixVersions" :
                case "customfield_10262":
                    let version = {};
                    version.change = change;
                    version.addition = !!(change.to);
                    version.suppresion = !(version.addition);
                    version.replanification = replanification;
                    if (replanification){
                        versions.replanifications++;
                        if (version.suppresion){versions.deletes ++;}
                    }
                    if (version.addition){
                         versions.latest = _ujg_gft_math_fixversion_to_time(change.toString);
                         if (!versions.first){versions.first = versions.latest;}
                    }
                    versions.push(version);            
                    break;
            }            
        }
    }

    // Move local variables to issue object.
    issue.changelog.versions = versions;
    issue.changelog.transitions = transitions;
    issue.changelog.state_changes = last_pass_to;
}

/** Function to call on document load and by UJG.
 * @param {ujgAPI} __ujgAPI Universal Gadget Jira API object
 */
 function _ujg_gft_dashboard_bsdroad_control_panel(__ujgAPI){
    _ujgAPI = __ujgAPI; // Allow API to be available globally.
    _ujg_gft_dashboard_bsdroad_load_select_boxes();

    google.charts.load('current', {'packages':['table']});
    google.charts.setOnLoadCallback(_ujg_gft_dashboard_bsdroad_main);
}

async function _ujg_gft_dashboard_bsdroad_load_select_boxes(){
    let cache = new JiraCachedDB();
    await cache.open();
    
    let statuses = (await cache.raw_request("/project/BSDROAD/statuses")).statuses;

    for (let j=0; j<statuses.length; j++){
        let option = document.createElement('option');
        option.value = encodeURIComponent(statuses[j].untranslatedName);
        option.text = statuses[j].name.toUpperCase();
        option.selected = "selected";
        $$('status').add(option);
    } 

}

/** Main entry point called after google charts load. No return or parameter is required
 */
async function _ujg_gft_dashboard_bsdroad_main(){
    _ujg_gft_dashboard_bsdroad_main_query = (await _ujg_gft_data_load_jira_dataset("project = BSDROAD AND status NOT IN (Backlog, Descartada, 'On Hold') AND issuetype in (epic) AND createddate >= startofday(-12M)", _ujg_gft_dashboard_bsdroad_add_issue_calculations));
    _ujg_gft_dashboard_bsdroad_refilter();
}

/** Refilter and refresh calculated information based on HTML Option value.
 * @param {DOMDiv} div div element containing options with the possible filters and values.
 */
 async function _ujg_gft_dashboard_bsdroad_refilter(div){
    let options_filter = [];
    let subfilter = [];
    let excluded_status = ['Done','Producción'];
    subfilter[1] = `status IN (${excluded_status.join()})`;
    subfilter[2] = `status NOT IN (${excluded_status.join()})`;

    // Get all select elements from the filter bar if we have one
    if (div){
        let include_blank = false;
        let selects = div.getElementsByTagName('select');
        for (let i=0; i < selects.length; i++){
            let select = selects[i];
            let values = [];
            for (let j=0; j < select.selectedOptions.length; j++){
                let option = select.selectedOptions[j];
                if (option.selected){
                    if (option.value == ""){
                        include_blank = true;
                    }else{
                        values.push(option.value);
                    }
                }
            }
            let field_name = select.id;
            if (include_blank && values.length == 0){
                options_filter.push(`${field_name} IS EMPTY`);
            } else if (include_blank && values.length > 0){
                options_filter.push(`(${field_name} IS EMPTY OR ${field_name} IN ('${values.join("','")}'))`);
            }else{
                options_filter.push(`(${field_name} IN ('${values.join("','")}'))`);
            }
        }
    }

    let option_joint = options_filter.length > 0 ? " AND " : "";
    let option_filter = options_filter.join(" AND ");
    _ujg_gft_dashboard_bsdroad_control_panel_refresh('td_2', `${subfilter[2]}${option_joint}${option_filter}`);
    _ujg_gft_dashboard_bsdroad_average_time_by_state_print(`${option_filter}`);
}

/** Apply filters and refresh screen information.
 * @param {string} jql JQL Query to refilter main query.
 */
async function _ujg_gft_dashboard_bsdroad_control_panel_refresh(td, jql){
    let dataset = (await _ujg_gft_data_load_filter_jira_dataset(_ujg_gft_dashboard_bsdroad_main_query, jql));
    _ujg_gft_dashboard_bsdroad_add_global_calculations(dataset);

    if (dataset.calculations){
        let calcs = dataset.calculations;
        $$(`_ujg_gft_data_${td}_10`).innerHTML = percent(calcs.planificado / calcs.count);
        $$(`_ujg_gft_data_${td}_11`).innerHTML = `<a href="${JQL_LINK}${dataset.jql}" target="_blank">${calcs.count}</a>`;
        $$(`_ujg_gft_data_${td}_12`).innerHTML = `${calcs.planificado} (${percent(calcs.planificado/calcs.count)})`;
        $$(`_ujg_gft_data_${td}_13`).innerHTML = `${calcs.replanificado} (${percent(calcs.replanificado/calcs.planificado)})`;
        $$(`_ujg_gft_data_${td}_14`).innerHTML = calcs.replanificado > 0 ? Math.round(calcs.dias_replanificado / calcs.replanificado, 0) : 0 ;
        $$(`_ujg_gft_data_${td}_15`).innerHTML = percent(calcs.predictibilidad / calcs.count);
        $$(`_ujg_gft_data_${td}_20`).innerHTML = percent(calcs.time2market_list.length / calcs.count);
        $$(`_ujg_gft_data_${td}_21`).innerHTML = `<a href="${JQL_LINK}id in (${calcs.time2market_list.join(",")})" target="_blank">${calcs.time2market_list.length}</a>`;
        $$(`_ujg_gft_data_${td}_22`).innerHTML = Math.round(calcs.time2market / calcs.time2market_list.length, 0) + 30;
    }else{
        $$(`_ujg_gft_data_${td}_10`).innerHTML = "";
        $$(`_ujg_gft_data_${td}_11`).innerHTML = "";
        $$(`_ujg_gft_data_${td}_12`).innerHTML = "";
        $$(`_ujg_gft_data_${td}_13`).innerHTML = "";
        $$(`_ujg_gft_data_${td}_14`).innerHTML = "";
        $$(`_ujg_gft_data_${td}_15`).innerHTML = "";
        $$(`_ujg_gft_data_${td}_20`).innerHTML = "";
        $$(`_ujg_gft_data_${td}_21`).innerHTML = "";
        $$(`_ujg_gft_data_${td}_22`).innerHTML = "";
    }    
}

/** Print a table with the average times an issue remains on each state. 
 * @param {*} dataset Set of issues to use as calculation data.
 */
async function _ujg_gft_dashboard_bsdroad_average_time_by_state_print(jql){
    // Local variable to store all possible allowed transitions cumulative information.
    let transitions = {};
    let dataset = (await _ujg_gft_data_load_filter_jira_dataset(_ujg_gft_dashboard_bsdroad_main_query, jql));
    _ujg_gft_dashboard_bsdroad_add_global_calculations(dataset);

    // Add transitions cumulative calculations for each possible transition.
    for (let i=0; i < dataset.issues.length; i++){
        let issue = dataset.issues[i];
        // Iterate trough transitions in the changelog.
        for (let key in issue.changelog.transitions){
            let transition = issue.changelog.transitions[key];
            let cumulative = {};
            // If transition dont exists on transitions list, initialize it.
            if ( ! transitions[transition.id]){
                cumulative.list = [];
                cumulative.count = 0;
                cumulative.sum = 0;
                cumulative.older = new Date();
                cumulative.from = transition.from;
                cumulative.to = transition.to;
            }else{
                cumulative = transitions[transition.id];
            }
            // Acumulate calculations and store the oldest value of each.
            cumulative.list.push(issue.id);
            cumulative.count += 1;
            cumulative.sum += !isNaN(transition.duration) ? transition.duration : ((new Date()) - transition.start ) / MMS2DAYS ; // If is not finished use NOW.
            if (transition.start < cumulative.older){ cumulative.older = transition.start;}
            transitions[transition.id] = cumulative;
        }
    }

    // Initialize table headers.
    let dataArray = [[
        {type: 'string', label: 'De'},
        {type: 'string', label: 'A'},
        {type: 'number', label: 'Cuenta'},
        {type: 'number', label: 'Días'},
        {type: 'number', label: 'Media'}
    ]];

    // Store unfiltered states into the data array used by the table.
    for (let id in transitions){
        let transition = transitions[id];
        dataArray.push([
            transition.from.string,
            transition.to.string,
            {v: transition.count, f: `<a href="${JQL_LINK}id in (${(transition.list.join(","))})" target="_blank">${transition.count}</a>`},
            Math.round(transition.sum, 0),
            Math.round(transition.sum / transition.count, 0)
        ]);
    }

    // Prepare data on required google format, destinatino div in DOM and draw the table.
    let dataTable = google.visualization.arrayToDataTable(dataArray);
    let table = new google.visualization.Table(document.getElementById('_ujg_gft_average_time_by_state'));
    table.draw(dataTable, {showRowNumber: false, width: '90%', height: '100%', allowHtml:true});  
}

/** Add attributes with needed calculations to the issue object. This mus be called using .call and not directly.
 * @param {object} issue Issue id in 'this' array.
 * @returns the same issue with the attirbute calculations filled.
 */
function _ujg_gft_dashboard_bsdroad_add_issue_calculations(issue){
    issue.calculations = {};

    // Get issue state changes.
    _ujg_gft_statistics_analyze_changelog(issue);
    issue.calculations.time2market = _ujg_gft_math_calculate_time2market(issue.changelog); 
    issue.calculations.planificaciones = issue.changelog.versions.length;

    if (issue.calculations.planificaciones == 0 || issue.fields.fixVersions.length == 0) {
        issue.calculations.planificado = 0;
        issue.calculations.replanificado = 0;
        issue.calculations.dias_replanificado = 0;
        issue.calculations.predictibilidad = 0;
    }else if (issue.calculations.planificaciones == 1 ) {
        issue.calculations.planificado = 1;
        issue.calculations.replanificado = 0;
        issue.calculations.dias_replanificado = 0;
        issue.calculations.predictibilidad = 1;
    }else {
        issue.calculations.planificado = 1;
        issue.calculations.replanificado = 1;
        issue.calculations.dias_replanificado = (issue.changelog.versions.latest - issue.changelog.versions.first) / MMS2DAYS;
        issue.calculations.predictibilidad = issue.changelog.versions.replanifications > 0 ?  1 / issue.changelog.versions.replanifications : 1;
    }
}

function _ujg_gft_dashboard_bsdroad_add_global_calculations(dataset){
   // Initialize global counters values if not exists.
    dataset.calculations = {};
    dataset.calculations.count = 0;
    dataset.calculations.planificado = 0;
    dataset.calculations.replanificado = 0;
    dataset.calculations.dias_replanificado = 0;
    dataset.calculations.predictibilidad = 0;
    dataset.calculations.time2market = 0;
    dataset.calculations.time2market_list = [];

   // Aggregate values.
   for (let i=0; i<dataset.issues.length; i++){
        let issue = dataset.issues[i];
        dataset.calculations.count += 1;
        dataset.calculations.planificado += issue.calculations.planificado;
        dataset.calculations.replanificado += issue.calculations.replanificado;
        dataset.calculations.dias_replanificado += Math.abs(issue.calculations.dias_replanificado);
        dataset.calculations.predictibilidad += issue.calculations.predictibilidad;
        if (issue.calculations.time2market > 0 ){
            dataset.calculations.time2market += issue.calculations.time2market;
            dataset.calculations.time2market_list.push(issue.id);
        }
    }
}