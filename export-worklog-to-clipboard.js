//define("_ujgEXPORTWORKLOGTOCLIPBOARD", [], function() {let fn = function(API) { _ujg_gft_export_worklog_to_clipboard(API); }; return fn;});
var onmemory_info = '';
EPICS_FILTER = '10921';
ISSUE_FILTER = '10920';

/**
 * Generic function to show errors.
 * @param {string} data Response JSON from request
 */
function _ujg_gft_generic_onError(data){
	console.debug(data);
	_ujg_gft_statusText(`<span style='color:red'>Error loading data: </span>${JSON.parse(data).errorMessages}`);
	_ujgAPI.resize();
}

/**
 * Function to call on document load.
 * @param {ujgAPI} __ujgAPI Universal Gadget Jira API object
 */

function _ujg_gft_export_worklog_to_clipboard(__ujgAPI){
	_ujgAPI = __ujgAPI;
	_ujgAPI.resize();
}

/**
 * Update status area with the text in param.
 * @param {string} text Text to show.
 */
function _ujg_gft_statusText(text){
	document.getElementById('status').innerHTML = text;
}

/**
 * Show a status message and a progress bar acording to the current process status.
 * @param {integer} current Number of items processed
 * @param {integer} total  Total items to process
 * @param {string} label Type of item to status text.
 */
function _ujg_gft_download_generic_status(current, total, label)
{
	let percent = Math.floor(current * 100 / total);
	_ujg_gft_statusText(`<div style='color:orange'>${label} extraction at ${percent}% (${current} of ${total})</div>`);
	document.getElementById('_ujg_gft_progress_bar').style.width = `${percent}%`;
}

/**
 * Download and process workload to browser memory.
 **/
async function _ujg_gft_download_workload()
{
	document.getElementById('_ujg_gft_btn_copy').disabled=true;
	onmemory_info = "";

	epics_searchUrl = `/rest/api/latest/search?fields=summary,assignee,status,customfield_10110&jql=filter=${EPICS_FILTER}`;
	issues_searchUrl= `/rest/api/latest/search?fields=summary,assignee,status,labels,timespent,timeestimate,customfield_10008,customfield_10259,customfield_10260&jql=filter=${ISSUE_FILTER}`;

	_ujg_gft_statusText("<div style='color:orange'>Starting data extraction.</div>");
	jql_response = await _ujg_gft_download_full_JQL (epics_searchUrl, _ujg_gft_download_generic_status, "Epics");
	epics_dictionary = _ujg_gft_epics_to_dictionary(jql_response);

	_ujg_gft_statusText("<div style='color:green'>Epics extracted. Extracting Issues</div>");
	issues_searchUrl = _ujg_gft_apply_epics_prefilter(issues_searchUrl, epics_dictionary);
	jql_response = await _ujg_gft_download_full_JQL (issues_searchUrl, _ujg_gft_download_generic_status, "Issues");
	
	_ujg_gft_statusText("<div style='color:orange'>Issues extracted. Processing information</div>");
	issues_csv = _ujg_gft_extract_workload_csv_issues(jql_response, epics_dictionary);

	_ujg_gft_statusText("<div style='color:green'>Ready to download</div>");
	document.getElementById('_ujg_gft_btn_copy').disabled=false;
	onmemory_info = issues_csv;
}

/**
 * Add a prefiltering in the JQL to only download issues with the right parent epics.
 * @param {string} issues_searchUrl Issues JQL url.
 * @param {dictionary} epics_dictionary Dictionary of all epics related to this export.
 * @returns The issues_searcUrl with the epics filter injected.
 */
function _ujg_gft_apply_epics_prefilter(issues_searchUrl, epics_dictionary)
{
	orderby_split = issues_searchUrl.split("+ORDER+BY+");
	orderby_split[0] = orderby_split[0] + `+AND+cf[10008]+IN+(${Object.keys(epics_dictionary).join(",")})`;
	return orderby_split.join("+ORDER+BY+");
}

/**
 * Convert a list of epics to a indexed dictionary by epic key.
 * @param {*} jql_response List of epics
 * @returns Epics dictionary indexed.
 */
function _ujg_gft_epics_to_dictionary(jql_response){
	let dictionary = {};
	for (let i=0; i< jql_response.total; i++){
		dictionary[jql_response.issues[i].key] = jql_response.issues[i];
	}
	return dictionary;
}

/**
 * Convert a jql_response to a csv array.
 * @param {object} jql_response Response from the request.
 * @param {dictionary} epics_dictionary dictionary to validate if the issue is related to any epic.
 * @returns 
 */
function _ujg_gft_extract_workload_csv_issues(jql_response, epics_dictionary)
{
	let csv_file= [];
	csv_file.push(_ujg_gft_extract_workload_csv_issue_headers());
	let extracted_issue;
	for (let i=0; i< jql_response.total; i++){
		extracted_issue = _ujg_gft_extract_workload_csv_issue(jql_response.issues[i], epics_dictionary);
		if (extracted_issue) {csv_file.push(extracted_issue);}
		else {
			console.log(extracted_issue);}
	}
	return csv_file.join("\n");
}

/**
 * Returns csv headers.
 * @returns CSV register with the headers.
 */
function _ujg_gft_extract_workload_csv_issue_headers()
{
	let csv = [];
	csv.push('Assignee'); // Assignee
	csv.push('Labels'); // Labels
	csv.push('Issue'); // Issue Key
	csv.push('Summary'); // Issue summary
	csv.push('Status'); // Issue status
	csv.push('Spent'); // Time spent (hours)
	csv.push('Remaining'); // Time remaining (hours)
	csv.push('Start'); // Target Start Date
	csv.push('End'); // Target End Date
	csv.push('Epic Key'); // Epica key
	csv.push('Epic'); // Description
	csv.push('PO' ); // PO
	csv.push('Epic Status'); // Status
	csv.push('Size MKT'); // Size MKT
	return csv.join(";");

}

/**
 * Extract issue data and convert to a csv register
 * @param {object} issue issue to convert.
 * @param {dictionary} epics_dictionary dictionary to search issue parent epic.
 * @returns csv register.
 */
function _ujg_gft_extract_workload_csv_issue(issue, epics_dictionary)
{
	let csv = [];
	let parent_epic = epics_dictionary[issue.fields.customfield_10008];
	if (parent_epic){
		csv.push(issue.fields.assignee ? issue.fields.assignee.displayName : null); // Assignee
		csv.push(issue.fields.labels.join(" ")); // Labels
		csv.push(issue.key); // Issue Key
		csv.push(issue.fields.summary); // Issue summary
		csv.push(issue.fields.status.name); // Issue status
		csv.push((issue.fields.timespent / 3600).toLocaleString()); // Time spent (hours)
		csv.push((issue.fields.timeestimate / 3600).toLocaleString()); // Time remaining (hours)
		csv.push(issue.fields.customfield_10259); // Target Start Date
		csv.push(issue.fields.customfield_10260); // Target End Date
		csv.push(parent_epic.key); // Epica key
		csv.push(parent_epic.fields.summary); // Description
		csv.push(parent_epic.fields.assignee ? parent_epic.fields.assignee.displayName : null); // PO
		csv.push(parent_epic.fields.status.name); // Status
		csv.push(parent_epic.fields.customfield_10110.value); // Size MKT
		return csv.join(";");
	}else{
		return null;
	}
}

/**
 * Convert csv on a string into a browser downloadable file.
 * @param {*} csv_on_string contents of the csv
 * @param {*} filename filename to download.
 */
function _ujg_gft_download_csv(csv_on_string, filename=null){
	var hiddenElement = document.createElement('a');
    hiddenElement.href = 'data:text/csv;charset=utf-8,%EF%BB%BF' + encodeURI(csv_on_string);
    hiddenElement.target = '_blank';  
	filename = filename ? filename : 'jira-extraction.csv';
    hiddenElement.download = filename;  
    hiddenElement.click();  
}

/**
 * Download query from JIRA API
 * @param {string} queryURL URL to download
 * @param {function} statusFunction Function to call during page downloads
 * @param {string} statusLabel Label of the status function.
 * @returns object with the jira request result
 */
async function _ujg_gft_download_full_JQL(queryURL, statusFunction = null, statusLabel = null)
{
    let responseJSON = "";
    let jql_result = Object.create( {} );
    let maxResults = 10000;
    jql_result.issues = Object.create( {} );
    jql_result.issues.length = 0;
    jql_result.total = 1; // set to 1 to enter the bucle.

    // Load all issues of this JQL.    
    while (jql_result.issues.length<jql_result.total)
    {
        pURL = queryURL+`&maxResults=${maxResults}&startAt=${jql_result.issues.length}`;
        responseJSON = "";
        AP.request({
            url: pURL ,
            dataType: "json",
            success : function(server_response) {
                responseJSON = JSON.parse(server_response); },
            error :   function(server_response) {
                responseJSON = JSON.parse(server_response); _ujg_gft_generic_onError(server_response);
            }
        });
        
        while (responseJSON == ""){await new Promise(r => setTimeout(r));}

        if (responseJSON.errorMessages){
            break;
        }else{
            if (jql_result.issues.length == 0){
                jql_result = responseJSON;
            }else{
                jql_result.issues = jql_result.issues.concat(responseJSON.issues);
            }
        }

		// If a status function is indicated call it.
		if (statusFunction) {statusFunction(jql_result.issues.length, jql_result.total, statusLabel);}
    }
	return jql_result;
}