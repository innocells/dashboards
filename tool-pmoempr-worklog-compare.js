//define("_ujg_gft_tool_pmoempr_worklog_compare", [], function() {let fn = function(API) { _ujg_gft_tool_pmoempr_worklog_compare(API); }; return fn;});
/** Shortcut for document.getElementById function
 * @param {string} id 
 * @returns Object with the required id.
 */
 function $$ (id) {return document.getElementById(id);}

 /** Function to call on document load and by UJG, loads google charts libraries and show body.
 * @param {ujgAPI} __ujgAPI Universal Gadget Jira API object
 */
function _ujg_gft_tool_pmoempr_worklog_compare(__ujgAPI){
    _ujgAPI = __ujgAPI; // Allow API to be available globally.
    google.charts.load('current', {'packages':['table']});
    google.charts.setOnLoadCallback( function(){ $$('ujg_body').style.visibility = "visible"; });
}var _ujgAPI;

/**
 * Carga archivo CSV, procesalo y muestra el resultado. Llamada principal.
 * @param {*} fileInput DOM file input element
 */
async function ujg_upload_file(fileInput) {
    // Query cache.
    let cache = new JiraCachedDB();
    await cache.open();
    
    if(fileInput.files[0] == undefined) {
        console.error("No file specified", err);
    }

    var reader = new FileReader();
    reader.onload = async function(ev)   { 
        $$('ujg_body').className="spinner";
        await ujg_process_csv_protected(ev.target.result, cache);
        $$('ujg_body').className="";
        $$('form_csv_upload').innerHTML="";
        _ujgAPI.resize();
    };
    reader.onerror = function(err) { console.error("Failed to read file", err);};

    reader.readAsArrayBuffer(fileInput.files[0]);    
}

/**
 * Call csv processing but in a protected environment with generic error handling.
 * @param {*} content 
 * @param {*} cache 
 */
async function ujg_process_csv_protected(content, cache){
    try{
        await ujg_process_csv(content, cache); 
    }catch (E){
        switch (E.name){
            case 'TypeError':
                message = "Se ha producido un error al procesar el archivo CSV.<br>Compruebe que el formato es el esperado.";
                break;
            default:
                message = `Unexpected error: ${E.message}`;
        }
        $$('google_table_div').innerHTML= `<span style='color:red; font-weight: bold'>${message}</span>`;
        console.error(E.stack);
    }    
}

/**
 * Procesa el archivo CSV:
 * @param {*} content 
 */
async function ujg_process_csv(content, cache){
    // Store CSV into a dictionary.
    const csv = ujg_csv_to_dictionary(new TextDecoder("utf-8").decode(content), 1);

    // Prepare data headers.
    let headers = [
        {label: 'Issue', type: 'string'},
        {label: 'Assignee', type: 'string'},
        {label: 'End', type: 'date'},
        {label: 'Spent', type: 'number'},
        {label: 'Status', type: 'number'}
    ];

    // Search all issues from PMOEMPR with HDEV value.
    const jql =  await cache.JQLQuery("Project = PMOEMPR AND issuetype=(Timetrackings) AND HDEV-Entrega-PJ IS NOT EMPTY AND HDEV-Entrega-PJ !~ 'N/A' AND timespent > 0"); 
    let innocells_store = {};

    // Store issues into a dictionary based on HDEV value as key and acumulate.
    for (let i=0; i<jql.total; i++){
        cache.issue(jql.issues[i], null, function(issue, innocells_store){
            let key = issue.fields.customfield_10227;
            if ( !(key in innocells_store) ){ // Initialize key if not exists.
                let item = {};
                item.issues = [];
                item.issueKeys = [];
                item.total = 0;
                item.timespentS = 0;
                item.timespentH = 0;
                innocells_store[key] = item;
            }         
            innocells_store[key].total++;
            innocells_store[key].issues.push(issue);
            innocells_store[key].issueKeys.push(issue.key);
            innocells_store[key].timespentS += issue.fields.timespent;
            innocells_store[key].timespent = Math.round(innocells_store[key].timespentS / 60 / 60);
        }, innocells_store);
    }
    await cache.flush();
    
    // Search for non-matching issues and store in a jira "like" dictionary structure.
    let differences = {};
    differences.issues = [];
    for(let key in csv){
        let timespent;
        let included_issues;
        let current_row = csv[key];

        // Convert value of Total (string) to => TotalNumber (number)
        csv[key].TotalNumber = 1 * csv[key].total.trim().replace("h","").replace("H","");

        // Validate if project exists and if timespent is equally.
        if ( ! (current_row.project in innocells_store) ){
            timespent = 0;
            included_issues = [];       
        }else{
            let innocells_timespent = innocells_store[current_row.project].timespent;
            if ( csv[key].TotalNumber != innocells_timespent){
                timespent = innocells_timespent;
                included_issues = innocells_store[current_row.project].issues;
            }
        }
        
        // If included_issues, store it in results.
        if (included_issues){
            let issue = {};
            issue.fields = {};
            issue.key = current_row.project;
            issue.self = `https://hdev.bancsabadell.com/rest/api/latest/issue/${issue.key}`;
            issue.fields.Total = csv[key].TotalNumber;
            issue.fields.timespent = timespent;
            issue.child = included_issues;
            differences.issues.push(issue);
        }
    }

    // Set generic object properties.
    differences.startAt = 0;
    differences.maxResults = 100;
    differences.expand = "";
    differences.total = differences.issues.length;

    // Draw the table with the results.
    if (differences.total  > 0) {
        let headers = [{label:'Clave', field:'key', type:'string'}, 
                        {label:'Total', field:'fields.Total', type:'string'},
                        {label:'Timespent', field:'fields.timespent', type:'number'},
                        {label:'Imputaciones', field:'child', type:'string'}];
        let options = { showRowNumber: false, 
                        width: '100%',
                        allowHtml: 'true',
                        page: 'enabled',
                        pageSize: '20'};
        ujg_jira_jql_to_google_table(headers, differences, 'google_table_div', options);
    }
}

/**
 * Print a generic jira array of issues into a pre-formated google chart table.
 * @param {*} headers Headers definition with at least label and type.
 * @param {*} jql_result Jira list of issues.
 * @param {*} table_element_id DOM element to store the table
 * @param {*} options Optional settings to pass to the table.
 */
function ujg_jira_jql_to_google_table(headers, jql_result, table_element_id, options=null){
  
    // Prepare server base url.
    let server_base = `https://${jql_result.issues[0].self.split("/")[2]}`;

    // Prepare arrayData to store information, and store headers.
    let arrayData = [];
    arrayData.push(headers);

    // Extract jql results and store in 
    for (let i=0; i<jql_result.total; i++){
        let row = [];
        for (let j=0; j<headers.length; j++){
            let header = headers[j];
            switch (header.field){
                case "child":
                    if (jql_result.issues[i].child.length > 0){
                        let child_server_base = `https://${jql_result.issues[i].child[0].self.split("/")[2]}`;
                        let keys = [];
                        for (let j=0; j<jql_result.issues[i].child.length; j++){
                            keys.push(jql_result.issues[i].child[j].key);
                        }
                        let jql = `${child_server_base}/issues/?jql=key IN (${keys.join(",")})`;
                        row.push(`<a href="${jql}" target="_blank">🔍</a>`);
                    }else{
                        row.push("");
                    }
                    break;
                default:
                    if ( header.field.substring(0, 7) == "fields." ){
                        row.push(jql_result.issues[i].fields[header.field.substring(7)]);
                    }else if (header.field == 'key'){
                        let key = jql_result.issues[i].key;
                        row.push( {v: key, f: `<a href="${server_base}/browse/${key}" target="_blank">${key}</a>`},);
                    }else{
                        row.push(jql_result.issues[i][header.field]);
                    }                                
            }

        }
        arrayData.push(row);
    }


    // Prepare data on required google format, destinatino div in DOM and draw the table.
    let dataTable = google.visualization.arrayToDataTable(arrayData);
    let table = new google.visualization.Table(document.getElementById(table_element_id));
    table.draw(dataTable, options);
}

/**
 * Convert a CSV string into a dictionary structure.
 * @param {string} csv_text The whole CSV as text with headers, lines separated by \n , fields by ';'.
 * @param {integer} keyFieldPosition Position of the field used as dictionary key.
 * @param {boolean} case_sensitive Optional. Set if headers mus be 
 * @returns dictionary of dictionaries referenced by keyFieldPositon and headers field name.
 */
function ujg_csv_to_dictionary(csv_text, keyFieldPosition, case_sensitive=false){
    let result = {};
    const csv_lines = csv_text.replace("\r\n","\n").replace("\r","\n").split("\n");
    const headers = (case_sensitive ? csv_lines[0] :csv_lines[0].toLocaleLowerCase()).split(";");
    for (let i=1; i<csv_lines.length; i++){
        let fields = csv_lines[i].split(";");
        if (fields[keyFieldPosition-1]){
            let key = fields[keyFieldPosition-1];
            result[key] = {};
            for (let j=0; j<headers.length; j++){
                result[key][headers[j]] = fields[j];
            }
        }
    }
    return result;
}