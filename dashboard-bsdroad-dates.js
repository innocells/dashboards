/* jshint node: true */
/* globals JiraCachedDB: false */
/* globals google: false */
/* globals document: false */
/* jshint -W097 */
'use strict';
const SERVER = "https://innocells.atlassian.net";

//define("_ujgCHARTBSDROADDATES", [], function() {let fn = function(API) { _ujg_gft_chart_bsdroad_dates(API); }; return fn;});
const MMS2DAYS = 86400000; // Conversion factor from milliseconds to days.
var _exportView; // Global variable to be available to downloads.
var _ujgAPI; // Global acces to Universal Jira Gadget API.

/**
 * Generic function to show errors.
 * @param {string} data Response JSON from request
 */
function _ujg_gft_generic_onError(data){
    console.debug(data);
    _ujg_gft_statusText(`<span style='color:red'>Error cargando los datos: </span>${JSON.parse(data).errorMessages}`);
    _ujgAPI.resize();
}

/**
 * Update status area with the text in param.
 * @param {string} text Text to show.
 */
function _ujg_gft_statusText(text){
    document.getElementById('_ujg_gft_status_line').innerHTML = text;
}

/**
 * Show a status message and a progress bar acording to the current process status.
 * @param {integer} current Number of items processed
 * @param {integer} total  Total items to process
 * @param {string} label Type of item to status text.
 */
function _ujg_gft_progress_total(current, total, label){
    let percent = Math.floor(current * 100 / total);
    _ujg_gft_statusText(`<div style='color:orange'>Extracción de ${label} al ${percent}% (${current} de ${total})</div>`);
    document.getElementById('_ujg_gft_progress_bar').style.width = `${percent}%`;
}

/**
 * Function to call on document load.
 * @param {ujgAPI} __ujgAPI Universal Gadget Jira API object
 */
 function _ujg_gft_chart_bsdroad_dates(__ujgAPI){
    _ujgAPI = __ujgAPI;
    google.charts.load('current', {'packages':['table', 'controls', 'corechart']});
    google.charts.setOnLoadCallback(_ujg_gft_drawTable);
}

/**
 * Main funtion to draw the table with the information.
 */
async function _ujg_gft_drawTable(){
    // Query to API REST.
    let jql = "project=BSDROAD AND issuetype IN (epic) Order by updated";
    
    // Query cache.
    let cache = new JiraCachedDB();
    await cache.open();

    // Download candidate issues.
    _ujg_gft_statusText("Buscando épicas.");
    let JQLSearch =  await cache.JQLQuery(jql, _ujg_gft_progress_total, ['Issue ']);
     
    // Download issues changelogs.
    _ujg_gft_statusText("Descargando registro de cambios de las épicas.");
    let progress = 0;
    for (let i=0; i<JQLSearch.issues.length; i++)
	{
        let foundIssue = JQLSearch.issues[i];
        cache.issue(foundIssue,"changelog").then( 
            (response) => {
                JQLSearch.issues[i] = response;
                _ujg_gft_progress_total(progress++, JQLSearch.issues.length, "registro de cambios")
            }
        );
	}
    await cache.flush(); // Wait until async queue is read.

    // Remove status and download bar. Enable toolbar.
    document.getElementById('_ujg_gft_status_div').style.display="none";
    document.getElementById('dashboard_div').style.display="initial";

    // Initialize table headers.
    let dataArray = [[
            {type: 'string', label: 'Epica'},           // Epic link
            {type: 'string', label: 'Épica'},           // key
            {type: 'string', label: 'Estado'},          // status
            {type: 'string', label: 'Workstream'},      // customfield_10275
            {type: 'string', label: 'Configuración'},   // customfield_10081
            {type: 'string', label: 'Complejidad'},     // customfield_10144
            {type: 'string', label: 'Tipología'},       // customfield_10055
            {type: 'number', label: 'HLE'},             // customfield_10066
            {type: 'string', label: 'Solicitada'},      // customfield_10262
            {type: 'string', label: 'Version'},         // fixVersions
            {type: 'number', label: 'T2M'},             // 
            {type: 'date', label: 'CREATION'},          // 0
            {type: 'date', label: 'KONGO START'},     // 10195 - customfield_10273 - [F. Presentacion KongGo Start]
            {type: 'date', label: 'READY2DEFINE'},    // 10186
            {type: 'date', label: 'DEF.RETO+HLE'},    // 10187
            {type: 'date', label: 'KONGO KO'},        // 10188 - customfield_10148 - [F. Presentacion KonGo KO]
            {type: 'date', label: 'READY2ANALYSIS'},  // 10189 - customfield_10140 - [F. Presentacion PPR Analisis]
            {type: 'date', label: 'ANÁLISIS'},        // 10117 - customfield_10086 - [F. Inicio Analis]
            {type: 'date', label: 'KONGO R4B'},       // 10190 - customfield_10155 - [F. Presentacion KonGo R4B]
            {type: 'date', label: 'READY2DEV'},       // 10191 - customfield_10141 - [F. Presentacion PPR Ejecucion]
            {type: 'date', label: 'DESARROLLO'},      // 10111 - customfield_10087 - [F. Inicio Dev]
            {type: 'date', label: 'UAT'},             // 10148 - customfield_10165 - [F. Inicio ACC Planificada]
            {type: 'date', label: 'REGRESIÓN'},       // 10192
            {type: 'date', label: 'KONGO GO'},        // 10193 - customfield_10274 - [F. Presentacion KongGo Go]
            {type: 'date', label: '1L / BETA'},       // 10194
            {type: 'date', label: 'ROLL OUT'},        // 10134 - customfield_10280 - [F. Apertura clientes planificada]
            {type: 'number', label: 'ANLY%'},         // customfield_10070
            {type: 'number', label: 'DEV%'},          // customfield_10120
            {type: 'number', label: 'SEMANAS'}        // customfield_10067
        ]];

    // Process and store JSON recovered data from api.
    for (let i=0; i<JQLSearch.issues.length; i++) {
        dataArray.push(_ujg_gft_process_epic_information(JQLSearch.issues[i]));
    }

    // Initialize the dashboard.
    let filters = [];
    let f=0;
    let dashboard = new google.visualization.Dashboard(document.getElementById('dashboard_div'));

    // Table component.
    let chartElement = new google.visualization.ChartWrapper({
        'chartType': 'Table',
        'containerId': 'chart_div',
        'options': {
            'allowHtml':'true',
            'showRowNumber': 'true',
            'frozenColumns': 1,
            'page': 'enabled',
            'pageSize': '40',
            'sortColumn': '0',
            'sortAscending': 'false',
        }
    });
    
    // Filters
    filters[f++] = new google.visualization.ControlWrapper({
        'controlType': 'StringFilter',
        'containerId': 'filter_div_epic',
        'options': {
            'filterColumnLabel': 'Epica',
            'matchType': 'any',
            'caseSensitive': 'false',
            'ui': {label: '', labelSeparator: ''},
        }
    });

    filters[f++] = new google.visualization.ControlWrapper({
        'controlType': 'CategoryFilter',
        'containerId': 'filter_div_status',
        'options': {
            'filterColumnLabel': 'Estado',
            'matchType': 'any',
            'caseSensitive': 'false',
            'ui': {label: '', labelSeparator: '', allowNone: true, allowMultiple: false}
        }
    });

    filters[f++] = new google.visualization.ControlWrapper({
        'controlType': 'CategoryFilter',
        'containerId': 'filter_div_workstream',
        'options': {
            'filterColumnLabel': 'Workstream',
            'matchType': 'any',
            'caseSensitive': 'false',
            'ui': {label: '', labelSeparator: '', allowNone: true, allowMultiple: false}
        }
    });

    filters[f++] = new google.visualization.ControlWrapper({
        'controlType': 'CategoryFilter',
        'containerId': 'filter_div_configuration',
        'options': {
            'filterColumnLabel': 'Configuración',
            'matchType': 'any',
            'caseSensitive': 'false',
            'ui': {label: '', labelSeparator: '', allowNone: true, allowMultiple: false}
        }
    });

    filters[f++] = new google.visualization.ControlWrapper({
        'controlType': 'CategoryFilter',
        'containerId': 'filter_div_complejidad',
        'options': {
            'filterColumnLabel': 'Complejidad',
            'matchType': 'any',
            'caseSensitive': 'false',
            'ui': {label: '', labelSeparator: '', allowNone: true, allowMultiple: false}
        }
    });

    filters[f++] = new google.visualization.ControlWrapper({
        'controlType': 'CategoryFilter',
        'containerId': 'filter_div_tipologia',
        'options': {
            'filterColumnLabel': 'Tipología',
            'matchType': 'any',
            'caseSensitive': 'false',
            'ui': {label: '', labelSeparator: '', allowNone: true, allowMultiple: false}
        }
    });

    filters[f++] = new google.visualization.ControlWrapper({
        'controlType': 'NumberRangeFilter',
        'containerId': 'filter_div_hle',
        'options': {
          'filterColumnLabel': 'HLE'
        }
      });


    // Associate table with filter and paint it.
    let dataTable = google.visualization.arrayToDataTable(dataArray);
    let dataView = new google.visualization.DataView(dataTable);
    dataView.hideColumns([1]);
    dashboard.bind(filters, chartElement);
    dashboard.draw(dataView);

    // Prepare and publish export view.
    _exportView = new google.visualization.DataView(dataTable);
    _exportView.setColumns([
        {type:'string', sourceColumn:0, label: 'ID'},
        1,2,3,4,5,6,7,8,9,10,
        {type:'string', sourceColumn:11, label: 'CREATION'},
        {type:'string', sourceColumn:12, label: 'KONGO START'},
        {type:'string', sourceColumn:13, label: 'READY2DEFINE'},
        {type:'string', sourceColumn:14, label: 'DEF.RETO+HLE'},
        {type:'string', sourceColumn:15, label: 'KONGO KO'},
        {type:'string', sourceColumn:16, label: 'READY2ANALYSIS'},
        {type:'string', sourceColumn:17, label: 'ANÁLISIS'},
        {type:'string', sourceColumn:18, label: 'KONGO R4B'},
        {type:'string', sourceColumn:19, label: 'READY2DEV'},
        {type:'string', sourceColumn:20, label: 'DESARROLLO'},
        {type:'string', sourceColumn:21, label: 'UAT'},
        {type:'string', sourceColumn:22, label: 'REGRESIÓN'},
        {type:'string', sourceColumn:23, label: 'KONGO GO'},
        {type:'string', sourceColumn:24, label: '1L / BETA'},
        {type:'string', sourceColumn:25, label: 'ROLL OUT'},
    ]);
    document.getElementById('dashboard_div').style.visibility="visible";

    // Resize visualitzation board.
    _ujgAPI.resize();
}

/**
 * Converts JSON information of an issue to the corresponding 
 * @param {object} issue Issue object from api rest response.
 * @returns Google chart DataTable register.
 */
function _ujg_gft_process_epic_information(issue){
    let last_pass_to = {};

    // Store last pass for each status to a indexed dictionary.
    last_pass_to[0] = issue.fields.created;
    for (let i=0; i<issue.changelog.values.length; i++){
        let changes = issue.changelog.values[i];
        for (let j=0; j<changes.items.length; j++){
            let change = changes.items[j];
            if (change.fieldId=="status"){
                last_pass_to[change.to] = changes.created;
            }            
        }
    }

    // Format and return dictionary values to chart strings.
    return [
        _ujg_gft_format_cell_key(issue), 
        issue.key, 
        issue.fields.status ? issue.fields.status.name : "", 
        issue.fields.customfield_10275 ? issue.fields.customfield_10275.value : "",
        issue.fields.customfield_10081 ? issue.fields.customfield_10081.value : "",
        issue.fields.customfield_10144 ? issue.fields.customfield_10144.value : "",
        issue.fields.customfield_10055 ? issue.fields.customfield_10055.value : "",
        issue.fields.customfield_10066 ? issue.fields.customfield_10066 : 0,
        _ujg_gft_format_versions_array(issue.fields.customfield_10262),
        _ujg_gft_format_versions_array(issue.fields.fixVersions),
        _ujg_gft_calculate_time2market(last_pass_to),
        _ujg_gft_format_cell(last_pass_to[0]),
        _ujg_gft_format_cell(last_pass_to[10195], issue.fields.customfield_10273),
        _ujg_gft_format_cell(last_pass_to[10186]),
        _ujg_gft_format_cell(last_pass_to[10187]),
        _ujg_gft_format_cell(last_pass_to[10188], issue.fields.customfield_10148),
        _ujg_gft_format_cell(last_pass_to[10189], issue.fields.customfield_10140),
        _ujg_gft_format_cell(last_pass_to[10117], issue.fields.customfield_10086),
        _ujg_gft_format_cell(last_pass_to[10190], issue.fields.customfield_10155),
        _ujg_gft_format_cell(last_pass_to[10191], issue.fields.customfield_10141),
        _ujg_gft_format_cell(last_pass_to[10111], issue.fields.customfield_10087),
        _ujg_gft_format_cell(last_pass_to[10148], issue.fields.customfield_10165),
        _ujg_gft_format_cell(last_pass_to[10192]),
        _ujg_gft_format_cell(last_pass_to[10193], issue.fields.customfield_10274),
        _ujg_gft_format_cell(last_pass_to[10194]),
        _ujg_gft_format_cell(last_pass_to[10134], issue.fields.customfield_10280),
        issue.fields.customfield_10070 ? issue.fields.customfield_10070 : "--",
        issue.fields.customfield_10120 ? issue.fields.customfield_10120 : "--",
        issue.fields.customfield_10067 ? issue.fields.customfield_10067 : "--",
    ];
}


/**
 * Format the issue id cell with text and hyperlink
 * @param {object} issue Issue object from api rest response.
 * @returns Chart structure with value and format.
 */
function _ujg_gft_format_cell_key(issue){
    let shortId = issue.key.split("-")[1].padStart(4, "0");
    return {v:shortId, f:`<a target="blank" href="https://innocells.atlassian.net/browse/${issue.key}">${shortId}</a>`};
}

/**
 * Format the issue status cell.
 * @param {*} date Date when the issue entered this state.
 * @param {*} date_commitment Date commited to deliver (used to sort and class)
 * @returns String with the cell formatted text. With rag based on colour
 */
function _ujg_gft_format_cell(date, date_commitment=null){
    let dateToShow = date || '';
    let rag;
    let title='';
    let span_class = '_ujg_gft_rag_grey'; // No RAG

    // Only process rag if exists commited date.
    if (date_commitment){
        rag = parseInt((new Date(date) - new Date(date_commitment)) / MMS2DAYS); 
        switch (true){
            case rag<=0:
                span_class = '_ujg_gft_rag_green';
                break;
            case rag>0 && rag<=7:
                span_class = '_ujg_gft_rag_yellow';
                break;
            case rag>7:
                span_class = '_ujg_gft_rag_red';
                break;
        }
        title = `title='${date_commitment} (${rag})'`;

    }
    return {v:dateToShow.substr(0, 10), f:`<span class='${span_class}' ${title}>${dateToShow.substr(0, 10)}</span>`};
}

/**
 * Calculate the issue time2market based on pass on states.
 * @param {Array} last_pass_to 
 * @returns Time2Market on days, null if any of the required last_pass is not suplied.
 */
function _ujg_gft_calculate_time2market(last_pass_to){
    // Search the best start date possible.
    let t2m = null;
    let start_date = last_pass_to[10188];   // KONGO KO 
    
    // Search the best end date possible.
    let end_date = last_pass_to[10134];     // ROLL OUT

    // Calculate and return on days between start and end date (zero or negative is not in the domain).
    if (start_date && end_date){
        t2m = parseInt((new Date(end_date) - new Date(start_date)) / MMS2DAYS) ; 
        t2m = t2m > 0 ? t2m : null;
    }
    return t2m; 
}

function _ujg_gft_calculate_elapsed_percent(start_date, end_date, next_started){
    let percent = 0;

    if (next_started){
        percent = 1;
    }
    else{
        if (start_date && end_date){
            let start = new Date(start_date);
            let end = new Date(end_date);
            let now = new Date();
            if (end < start){
                percent = "ERR"; 
            }else if (end > now) {
                percent = (end - now) / (end - start);
            }else{
                percent = -0.5;
            }
        }
    }

    return `${Math.round(percent*100)}%`;
}

/**
 * Format a versions array field into printable strings.
 * @param {Array} versions_array 
 * @returns A string with the available versions separated by spaces in the array or an empty string if none exists.
 */
 function _ujg_gft_format_versions_array(versions_array)
 {
    let versions = [];
    if (versions_array){
        for (let i=0; i<versions_array.length; i++){
            versions.push(versions_array[i].name);
        }
    }
    return versions.join(" "); // Don't use comma to avoid problems with csv exports.
 }

/**
 * Export and download a google data view on CSV.
 * @param {Object} dataView Data to export
 * @param {string} filename optional file name.
 */
function _ujg_gft_download_google_dataView(dataView, filename=null){
    _ujg_gft_download_google_dataTable(dataView.toDataTable(), filename);
 }

/**
 * Export and download a google data table to CSV.
 * @param {Object} dataTable 
 * @param {string} filename 
 */
function _ujg_gft_download_google_dataTable(dataTable, filename=null){
    let csvFormattedDataTable = google.visualization.dataTableToCsv(dataTable);
    let header = [];
    for (let i=0; i<dataTable.bf.length; i++){
        header.push(dataTable.bf[i].label);
    }
    _ujg_gft_download_csv(header.join(";") + "\n" + csvFormattedDataTable.replaceAll(",",";"), filename);
}

/**
 * Convert csv on a string into a browser downloadable file.
 * @param {*} csv_on_string contents of the csv
 * @param {*} filename filename to download.
 */
 function _ujg_gft_download_csv(csv_on_string, filename=null){
	var hiddenElement = document.createElement('a');
    hiddenElement.href = 'data:text/csv;charset=utf-8,%EF%BB%BF' + encodeURI(csv_on_string);
    hiddenElement.target = '_blank';  
	filename = filename ? filename : 'jira-extraction.csv';
    hiddenElement.download = filename;  
    hiddenElement.click();  
}