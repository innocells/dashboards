/* jshint node: true */
/* globals JiraCachedDB: false */
/* globals google: false */
/* globals document: false */
/* jshint -W097 */
'use strict';
const SERVER = "https://innocells.atlassian.net";

//define("_ujgCHARTPMOEMPRDATES", [], function() {let fn = function(API) { _ujg_gft_chart_pmoempr_dates(API); }; return fn;});
const MMS2DAYS = 86400000; // Conversion factor from milliseconds to days.
var _exportView; // Global variable to be available to downloads.
var _ujgAPI; // Global acces to Universal Jira Gadget API.

/**
 * Generic function to show errors.
 * @param {string} data Response JSON from request
 */
function _ujg_gft_generic_onError(data){
    console.debug(data);
    _ujg_gft_statusText(`<span style='color:red'>Error cargando los datos: </span>${JSON.parse(data).errorMessages}`);
    _ujgAPI.resize();
}

/**
 * Update status area with the text in param.
 * @param {string} text Text to show.
 */
function _ujg_gft_statusText(text){
    document.getElementById('_ujg_gft_status_line').innerHTML = text;
}

/**
 * Show a status message and a progress bar acording to the current process status.
 * @param {integer} current Number of items processed
 * @param {integer} total  Total items to process
 * @param {string} label Type of item to status text.
 */
function _ujg_gft_progress_total(current, total, label){
    let percent = Math.floor(current * 100 / total);
    _ujg_gft_statusText(`<div style='color:orange'>Extracción de ${label} al ${percent}% (${current} de ${total})</div>`);
    document.getElementById('_ujg_gft_progress_bar').style.width = `${percent}%`;
}

/**
 * Function to call on document load.
 * @param {ujgAPI} __ujgAPI Universal Gadget Jira API object
 */
 function _ujg_gft_chart_pmoempr_dates(__ujgAPI){
    _ujgAPI = __ujgAPI;
    google.charts.load('current', {'packages':['table', 'controls', 'corechart']});
    google.charts.setOnLoadCallback(_ujg_gft_drawTable);
}

/**
 * Main funtion to draw the table with the information.
 */
async function _ujg_gft_drawTable(){
    // Query to API REST.
    let jql = "project=PMOEMPR AND issuetype = Epic AND (labels is EMPTY OR labels not in (oculto)) ORDER BY updated";
    
    // Query cache.
    let cache = new JiraCachedDB();
    await cache.open();

    // Download candidate issues.
    _ujg_gft_statusText("Buscando épicas.");
    let JQLSearch =  await cache.JQLQuery(jql, _ujg_gft_progress_total, ['Issue ']);
     
    // Download issues changelogs.
    _ujg_gft_statusText("Descargando registro de cambios de las épicas.");
    let progress = 0;
    for (let i=0; i<JQLSearch.issues.length; i++)
	{
        let foundIssue = JQLSearch.issues[i];
        cache.issue(foundIssue,"changelog").then( 
            (response) => {
                JQLSearch.issues[i] = response;
                _ujg_gft_progress_total(progress++, JQLSearch.issues.length, "registro de cambios");
            }
        );
	}
    await cache.flush(); // Wait until async queue is read.

    // Remove status and download bar. Enable toolbar.
    document.getElementById('_ujg_gft_status_div').style.display="none";
    document.getElementById('dashboard_div').style.display="initial";

    // Table configuration.
    let configuration = [
        {type: 'number', label: 'Epica', value: 'link', noexport: true},
        {type: 'string', label: 'Epica', value: 'key', novisible: true},
        {type: 'string', label: 'Nombre', value: 'summary'},
        {type: 'string', label: 'Estado', value: 'status', novisible: true},
        {type: 'string', label: 'Estado', value: 'status_button', noexport: true},
        {type: 'string', label: 'FixVersion', value: 'fixVersions'},

        {type: 'date', label: 'FEI Briefing', value: 'customfield_10366:date'},
        {type: 'date', label: 'FEF Briefing', value: 'customfield_10367:date'},
        {type: 'string', label: 'T. Briefing', value: 'statetime_10423'},

        {type: 'date', label: 'FEI Conceptualizacion', value: 'customfield_10368:date'},
        {type: 'date', label: 'FEF Conceptualizacion', value: 'customfield_10369:date'},
        {type: 'string', label: 'T. Conceptualizacion', value: 'statetime_10424'},

        {type: 'date', label: 'FEI  Rev. Conceptual', value: 'customfield_10384:date'},
        {type: 'date', label: 'FEF  Rev. Conceptual', value: 'customfield_10385:date'},
        {type: 'string', label: 'T. Rev. Conceptual', value: 'statetime_10425'},

        {type: 'date', label: 'FEI HLE + Valor. Análisis', value: 'customfield_10383:date'},
        {type: 'date', label: 'FEF HLE + Valor. Análisis', value: 'customfield_10295:date'},
        {type: 'string', label: 'T. HLE + Valor. Análisis', value: 'statetime_10436'},

        {type: 'date', label: 'FEI OK Budget Any', value: 'customfield_10378:date'},
        {type: 'date', label: 'FEF OK Budget Any', value: 'customfield_10377:date'},
        {type: 'string', label: 'T. OK Budget Any', value: 'statetime_10411'},

        {type: 'date', label: 'FEI Analysis', value: 'customfield_10086:date'},
        {type: 'date', label: 'FEF Analysis', value: 'customfield_10216:date'},
        {type: 'string', label: 'T. Analysis', value: 'statetime_10223'},

        {type: 'date', label: 'FEI OK Budget Dev', value: 'customfield_10370:date'},
        {type: 'date', label: 'FEF OK Budget Dev', value: 'customfield_10371:date'},
        {type: 'string', label: 'T. OK Budget Dev', value: 'statetime_10404'},

        {type: 'date', label: 'FEI Desarollo', value: 'customfield_10087:date'},
        {type: 'date', label: 'FEF Desarollo', value: 'customfield_10335:date'},
        {type: 'string', label: 'T. Desarollo', value: 'statetime_10111'},

        {type: 'date', label: 'FEI UAT', value: 'customfield_10165:date'},
        {type: 'date', label: 'FEF UAT', value: 'customfield_10349:date'},
        {type: 'string', label: 'T. UAT', value: 'statetime_10148'},

        {type: 'date', label: 'FEI Piloto', value: 'customfield_10374:date'},
        {type: 'date', label: 'FEF Piloto', value: 'customfield_10375:date'},
        {type: 'string', label: 'T. Piloto', value: 'statetime_10427'},

        {type: 'date', label: 'FEI Producción', value: 'customfield_10376:date'},
        {type: 'string', label: 'T. Dingle', value: 'f:dingle'},

    ];

    // Initialize table headers.
    let dataArray = [[]];
    for (let i=0; i<configuration.length; i++){
        dataArray[0].push ({type:configuration[i].type, label:configuration[i].label});
    }
    
    // Process and store JSON recovered data from api.
    for (let i=0; i<JQLSearch.issues.length; i++) {
        dataArray.push(_ujg_gft_process_epic_information(configuration, JQLSearch.issues[i]));
    }

    // Initialize the dashboard.
    let filters = [];
    let f=0;
    let dashboard = new google.visualization.Dashboard(document.getElementById('dashboard_div'));

    // Table component.
    let chartElement = new google.visualization.ChartWrapper({
        'chartType': 'Table',
        'containerId': 'chart_div',
        'options': {
            'allowHtml':'true',
            'showRowNumber': 'true',
            'frozenColumns': 2,
            'sortColumn': '0',
            'sortAscending': 'false',
            'cssClassNames': {'tableCell' : 'ujg_gft_table'},
        }
    });
    
    // Filters
    filters[f++] = new google.visualization.ControlWrapper({
        'controlType': 'StringFilter',
        'containerId': 'filter_div_epic',
        'options': {
            'filterColumnLabel': 'Nombre',
            'matchType': 'any',
            'caseSensitive': 'false',
            'ui': {label: '', labelSeparator: ''},
        }
    });

    // Associate table with filter and paint it.
    let dataTable = google.visualization.arrayToDataTable(dataArray);
    let dataView = new google.visualization.DataView(dataTable);
    for (let i=0; i<configuration.length; i++){
        if (configuration[i].novisible){
            dataView.hideColumns([i]);
        }
    }
    //dataView.hideColumns([1]); // Recorrer via bucle !!!!!!
    dashboard.bind(filters, chartElement);
    dashboard.draw(dataView);

    // Prepare and publish export view.
    _exportView = new google.visualization.DataView(dataTable);
    for (let i=0; i<configuration.length; i++){
        if (configuration[i].noexport){
            _exportView.hideColumns([i]);
        }
    }
    
    // Resize visualitzation board.
    document.getElementById('dashboard_div').style.visibility="visible";
    _ujgAPI.resize();
}

/**
 * Converts JSON information of an issue to the corresponding 
 * @param {object} issue Issue object from api rest response.
 * @returns Google chart DataTable register.
 */
function _ujg_gft_process_epic_information(configuration, issue){
    let last_pass_to = {};
    let first_pass_to = {};

    // Store first and last pass for each status to a indexed dictionary.
    first_pass_to[0] = issue.fields.created;
    last_pass_to[0] = issue.fields.created;
    for (let i=0; i<issue.changelog.values.length; i++){
        let changes = issue.changelog.values[i];
        for (let j=0; j<changes.items.length; j++){
            let change = changes.items[j];
            if (change.fieldId=="status"){
                if (!first_pass_to[change.to]){
                    first_pass_to[change.to] = changes.created;
                }
                last_pass_to[change.to] = changes.created;
            }      
        }
    }

    // Iterate configuration field by field.
    let result = [];
    for (let i=0; i<configuration.length; i++){
        let config = configuration[i].value;

        if (config.substr(0,12) == "customfield_") {
            let customfield = config.split(":");
            switch (customfield[1]){
                case 'date':
                    result.push(_ujg_gft_format_cell_date(issue.fields[customfield[0]]));
                    break;
                default:
                    result.push(issue.fields[customfield[0]] ? issue.fields[customfield[0]] : "");
            }

        }else if (config.substr(0,10) == "statetime_"){
            let datediff = _ujg_gft_math_datediff(first_pass_to[config.split("_")[1]],last_pass_to[config.split("_")[1]]);
            result.push(_ujg_gft_math_datediff_to_days(datediff));
        }else{
            switch (config){
                case 'id':
                    result.push(issue.id);
                    break;
                case 'self':
                    result.push(issue.self);
                    break;
                case 'key':
                    result.push(issue.key);
                    break;
                case 'link':
                    result.push({v:parseInt(issue.key.split("-")[1]), f:`<a target="blank" href="https://innocells.atlassian.net/browse/${issue.key}">${issue.key}</a>`});
                    break;
                case 'fixVersions':
                    result.push(_ujg_gft_format_cell_array(issue.fields.fixVersions));
                    break;
                case 'duedate':
                    result.push(_ujg_gft_format_cell_date(issue.fields.duedate));
                    break;
                case 'status':
                    result.push(issue.fields.status.name.toUpperCase());
                    break;
                case 'status_button':
                    result.push({v:issue.fields.status.id, f:`<span class='_ujg_gft_status_category_${issue.fields.status.statusCategory.id}'>${issue.fields.status.name.toUpperCase()}</span>`});
                    break;
                case 'f:dingle':
                    let dingle_ms = 0;
                    let dingle_states = ['10425','10436','10223','10111','10148','10427'];
                    for (let i=0; i<dingle_states.length; i++){
                        dingle_ms += Math.abs(_ujg_gft_math_datediff(first_pass_to[dingle_states[i]],last_pass_to[dingle_states[i]]));
                    }
                    result.push(_ujg_gft_math_datediff_to_days(dingle_ms));
                    break;
                default:
                    result.push(issue.fields[config]);
            }
        }
    }

    // Format and return dictionary values to chart strings.
    return result;
}

/**
 * Special formatting for dates cells.
 * @param {*} date Date to format.
 * @returns Google required date formated.
 */
function _ujg_gft_format_cell_date(date){
    return {v:date ? date : '0000/00/00 00:00', f:date ? date : ''};
}

/**
 * Format an array field into printable strings.
 * @param {Array} versions_array 
 * @returns A string with the available values separated by separator (space by default) in the array or an empty string if none exists. Don't use comma to avoid problems with csv exports.
 */
 function _ujg_gft_format_cell_array(array, separator=" "){
    let result = [];
    if (array){
        for (let i=0; i<array.length; i++){
            result.push(array[i].name);
        }
    }
    return result.join(separator); // Don't use comma to avoid problems with csv exports.
 }


/**
 * Calculate difference in ms between two dates.
 * @param {*} date_start Initial date
 * @param {*} date_end Final date
 * @returns Real number with the difference on time between two dates
 */
 function _ujg_gft_math_datediff(date_start, date_end){
     if (date_start && date_end){
        return Math.abs(new Date(date_end) - new Date(date_start));
     }else{
         return 0;
     }        
}

/**
 * Convert datediif millisecond into compelte days.
 * @param {*} milliseconds 
 * @returns 
 */
function _ujg_gft_math_datediff_to_days(milliseconds){
    let onDays = milliseconds / (1000 * 60 * 60 * 24);
    return (milliseconds > 0) ? Math.ceil( onDays )  : "";
}

/**
 * Export and download a google data view on CSV.
 * @param {Object} dataView Data to export
 * @param {string} filename optional file name.
 */
function _ujg_gft_download_google_dataView(dataView, filename=null){
    _ujg_gft_download_google_dataTable(dataView.toDataTable(), filename);
 }

/**
 * Export and download a google data table to CSV.
 * @param {Object} dataTable 
 * @param {string} filename 
 */
function _ujg_gft_download_google_dataTable(dataTable, filename=null){
    let csvFormattedDataTable = google.visualization.dataTableToCsv(dataTable);
    let header = [];
    for (let i=0; i<dataTable.bf.length; i++){
        header.push(dataTable.bf[i].label);
    }
    _ujg_gft_download_csv(header.join(";") + "\n" + csvFormattedDataTable.replaceAll(",",";"), filename);
}

/**
 * Convert csv on a string into a browser downloadable file.
 * @param {*} csv_on_string contents of the csv
 * @param {*} filename filename to download.
 */
 function _ujg_gft_download_csv(csv_on_string, filename=null){
	var hiddenElement = document.createElement('a');
    hiddenElement.href = 'data:text/csv;charset=utf-8,%EF%BB%BF' + encodeURI(csv_on_string);
    hiddenElement.target = '_blank';  
	filename = filename ? filename : 'jira-extraction.csv';
    hiddenElement.download = filename;  
    hiddenElement.click();  
}