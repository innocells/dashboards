/* Connect to server */
const DEFAULT_SERVER = "https://innocells.atlassian.net";
// move const SERVER to local JS file, if no server is defined, use default one.


/* Now in seconds */
_jug_EMULATOR_CACHE_ENABLED = -1; // Use -1 to disable
_jug_EMULATOR_CACHE_TTLs = _jug_EMULATOR_CACHE_ENABLED*60*60*1;

function nows(){ return Math.floor(Date.now() / 1000);}

String.prototype.hashCode = function() {
    var hash = 0;
    if (this.length == 0) {
        return hash;
    }
    for (var i = 0; i < this.length; i++) {
        var char = this.charCodeAt(i);
        hash = ((hash<<5)-hash)+char;
        hash = hash & hash; // Convert to 32bit integer
    }
    return hash.toString(16);
};

/* Jira Universal Gadget emulation */
function makeAjaxCall(args)
{
	var cache_hit = false;
	var cache_local = "";
	var auth = 'Basic ' + PAT;

	/* Set type as get if is not set */
	if (!args.type) {args.type = "GET";}

	/* Purge asyncronously whole cache to don't store unnecessary information . */
	let storage_item;
	for (var key in localStorage){
		storage_item = JSON.parse(localStorage.getItem(key));
		try{
			if (storage_item.expiration < nows()){
				localStorage.removeItem(key);
			}
		}catch (error) {}
	 }

	/** Prepare URL to open */
	if (args.url.substr(0, 4) != "http") { args.url = (typeof SERVER != 'undefined' ? SERVER : DEFAULT_SERVER) + args.url;}
	
	/* Validate if cache is ok and fresh. TTL=-1 disables any cache */
	if (_jug_EMULATOR_CACHE_TTLs > 0){
		cache_local = JSON.parse(localStorage.getItem(args.url.hashCode()));
		if (cache_local){ cache_hit = (cache_local.expiration > nows() && args.type == "GET");}
	}

	/* Response with data in cache or request to internet */
	if (cache_hit){/*Emulate async response but read from cache.*/
		console.debug("EMULATOR HIT: "+(cache_local.expiration-nows())+":" + args.url);
		setTimeout(function () {args.success(LZString.decompressFromUTF16(cache_local.response));});
	}else{/* Send a real http query to the server. */
		console.debug("EMULATOR MISS: "+ args.url);
		if (args.data){
			httpRequest(args.url, auth, args.type, args.success, args.error, args.data, args.contentType);
		}else{
			httpRequest(args.url, auth, args.type, args.success, args.error);
		}
		
	}

}

function httpRequest(url, authorization, operation, onSuccess, onError, data=null, contentType=null ){
	/* Prepare all request values, fallback, etc.. */
	let xhttp = new XMLHttpRequest();
	xhttp.onreadystatechange = onReadyStateChange;

	/* Store original URL */
	xhttp.requestURL = url;

	/* Set callback functions if they are set. */
	if (onSuccess) {xhttp.onSuccess = onSuccess;}
	if (onError) {xhttp.onError = onError;}

	/* Prepare asyncronous connection. */
	xhttp.open(operation, xhttp.requestURL, true);
	if (authorization != "") {xhttp.setRequestHeader("Authorization", authorization);}

	/* Send the request with JSON payload if exists. */
	if (data){
		if (contentType){xhttp.setRequestHeader("Content-Type", contentType);}
		xhttp.send(data);
	}else{
		xhttp.send();
	}
	
}

function onReadyStateChange(){
	if (this.readyState == 4){ //DONE
		switch (this.status){
			case 200: // Parse JSON and return reponse if successfull.
				/* Store response in the local cache. */
				var volatileCache = {};
				volatileCache.expiration = nows() + _jug_EMULATOR_CACHE_TTLs;
				volatileCache.response =  LZString.compressToUTF16(this.response);
				if (_jug_EMULATOR_CACHE_TTLs > 0){ // TTL -1 to disable it.
					try{localStorage.setItem(this.requestURL.hashCode(), JSON.stringify(volatileCache));}
					catch (error) {}
				}
				if (this.onSuccess != null){this.onSuccess(this.response);}
				break;
			case 201: // Post responds with 201, no cache management is required.
				if (this.onSuccess != null){this.onSuccess(this.response);}
				break;
			default: // Return errors if callback function is set.
				if (this.onError != null) 
				{
					this.source = this;
					this.onError(this);
				}
		}
	}
}

function fakeObj(){}
fakeObj.prototype.resize = function(){ return; };
fakeObj.prototype.makeAjaxCall = makeAjaxCall;
fakeObj.prototype.request = makeAjaxCall;
fakeObj.prototype.flag = function(args){ alert(args.body); console.log(args); return; };
var AP = new fakeObj();
var AJS = new fakeObj();
function define(){}