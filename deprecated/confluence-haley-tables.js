        const AVAILABLE_OPS = "sum,avg,count";
        const FIELDTYPES_DICTIONARY = {
            "string": "string",
            "list":"string", 
            "version":"string",
            "user": "string",
            "shortuser": "string",
            "status": "string",
            "labels": "string",
            "type": "string",
            "priority": "string",
            "sum": "number",
            "avg": "number",
            "count": "number",
        };

        function refresh_confluence_haley_component(){
            $$("board").innerHTML = "Cargando datos...";
            update_confluence_haley_component().catch((error) => {
                $$("error_bar").innerHTML = `Error Inesperado: ${error}`;
            });
        }

        async function update_confluence_haley_component(){
            // Prepare headers.
            let data = [];
            let headers = [];
            let list_fields_array = [];

            if (config.type == "table"){
                headers.push({id:'key', type:'string', type_real:'string', label: 'Id'});
            }

            let field_list = config['field_list'].split(",");
            for (let i=0; i<field_list.length; i++){
                let header = {};
                field = field_list[i].split(":");
                if (field[0].toLowerCase() != "key") {
                    header.id = field[0].search(/cf\[(.*)\]/) == -1 ? field[0].trim() : 'customfield_'+field[0].match(/\[(.*)\]/).pop();
                    header.type = FIELDTYPES_DICTIONARY[field[2].toLowerCase().trim()];
                    header.type_real = field[2].toLowerCase().trim();
                    header.label =  field[1].length>0 ? field[1] : field[0];
                    headers.push(header);
                    list_fields_array.push(header.id);
                }
            }
            data.push(headers);

            // Gather data.
            let search = await _chtmlm_gft_paginated_fetch(`/search?jql=${config['JQL']}&fields=${list_fields_array.join(",")}`, 'issues');

            // Draw component.
            switch (config['type']) {
                case "summary":
                    _chtml_gft_draw_summary_table(headers, data, search.issues, $$('board'));
                    break;
                case "pie":
                    _chtml_gft_draw_pie_chart(headers, data, search.issues, $$('board'));
                    break;
                case "table":
                    _chtmlm_gft_draw_data_table(headers, data, search.issues, $$('board'));
                    break;
                default:
                    $$('error_bar').innerText = "Error: Wrong component type selected";
            }
        }
        
        /**
         * 
         */
        function _chtmlm_gft_draw_data_table(headers, data, issues, div){
            for (let i=0; i<issues.length; i++){
                let issue = issues[i];
                let row = [];
                for (let r=0; r<headers.length; r++){
                    row.push(_chtmlm_gft_format_field(config, headers[r], issue));
                }
                data.push(row);
            }

            let table = new google.visualization.Table(div);
            let tableData = google.visualization.arrayToDataTable(data);
            let tableSettings =  {
                allowHtml:'true',
                showRowNumber: 'false',
                width: '100%',
                height: '100%',
            };                
            table.draw(tableData, tableSettings);
        }

        /**
         * Draw a sumarized table 
         */
        function _chtml_gft_draw_summary_table(headers, data, issues, div){
            // Calculate acumulative operations.
            data = _chtml_gft_sumarize_information(headers, data, issues);

            // Print sumarized table.
            let table = new google.visualization.Table(div);
            let dataTable = google.visualization.arrayToDataTable(data);
            let settings =  {
                allowHtml:'true',
                showRowNumber: 'false',
                width: '100%',
                height: '100%',
            };                
            table.draw(dataTable, settings);
        }

        /**
         * Draw a pie chart
         */
         function _chtml_gft_draw_pie_chart(headers, data, issues, div){
            // Calculate acumulative operations.
            data = _chtml_gft_sumarize_information(headers, data, issues, true);

            // Draw chart sumarized table.
            let chart = new google.visualization.PieChart(div);
            let dataTable = google.visualization.arrayToDataTable(data);
            let settings = { 
                pieSliceText: 'value',  
                legend: {position: 'top'}    
            };
            chart.draw(dataTable, settings);
        }


        /**
         */
         function _chtml_gft_sumarize_information(headers, data, issues, isChartLabel=false){
            // Group data based on the primary key.
            let grouped_data = {};
            for (let i=0; i<issues.length; i++){
                let pk;
                let row = [];
                let issue = issues[i];
                for (let r=0; r<headers.length; r++){
                    if (AVAILABLE_OPS.search(headers[r].type_real) > -1 || isChartLabel){ 
                        row.push(_chtmlm_gft_format_field(config, headers[r], issue).v);
                    }else{
                        row.push(_chtmlm_gft_format_field(config, headers[r], issue));
                    }
                }
                pk = _chtmlm_gft_generate_primary_key(headers, issue);
                if (!grouped_data[pk]){
                    grouped_data[pk] = [];
                }
                grouped_data[pk].push(row);
            }
            
            // Calculate incremental matematical operations.
            let result_data = [];
            for (let key in grouped_data) {
                let group = grouped_data[key];
                
                // Initialize headers.
                let row = group[0];
                for (let i=0; i<headers.length; i++){
                    if (AVAILABLE_OPS.search(headers[i].type_real) > -1){ 
                        row[i]=0;
                    }else{
                        // If we don't have any OP header, show error and exit.
                         if (i == headers.length - 1){
                            $$('error_bar').innerText = "No se ha especificado ningun cálculo a realizar.";
                            return 
                         }
                    }
                }
                
                // Do mathematics on grouped data.
                for (let i=0; i<group.length; i++) {
                    let item = group[i];
                    
                    for (let j=0; j<headers.length; j++){
                        let value = +parseFloat(item[j] ? item[j] : 0);
                        switch (headers[j].type_real){
                            case "sum":
                                row[j] += value;
                                break;
                            case "avg":
                                row[j] += (value / group.length);
                                break;
                            case "count":
                                row[j]  = group.length;
                                break;
                        }
                    }
                }
                data.push(row);
            }
            
            return data;
        }

        function _chtmlm_gft_generate_primary_key(headers, issue){
            let key = [];
            
            for (let i=0; i<headers.length; i++){
                let header = headers[i];
                if (AVAILABLE_OPS.search(header.type_real) == -1){
                    key.push(_chtmlm_gft_format_field(null, header, issue).v);
                }                
            }
            return key.join("|");
        }

        /**
         * 
         */
        function _chtmlm_gft_format_field(config, header, issue){
            let value = '';
            let hid = header.id;

            if (hid == 'key'){
                value = {v:issue.id, f:`<a target="blank" href="${config['server']}/browse/${issue.key}">${issue.key}</a>`};
            }else{
                let field = issue.fields[hid];
                switch (header.type_real) {
                    case "date":
                        value = field ? new Date(field) : new Date(0);
                        value = {v: value, f: field ? field : ""};
                        break;
                    case "list":
                        value = field ? field.value : "";
                        value = {v: value, f: value};
                        break;
                    case "version":
                        value = _chtmlm_gft_format_versions_array( field);
                        value = {v: value, f: value};
                        break;
                    case "user":
                        value = field ? {v: field.displayName, f: `<img width="24" src="${field.avatarUrls['24x24']}">&nbsp; ${field.displayName}`} : {v: "", f: ""};
                        break;
                    case "usericon":
                        value = field ? {v: field.displayName, f: `<img width="24" src="${field.avatarUrls['24x24']}" title="${field.displayName}">`} : {v: "", f: ""};
                        break;
                    case "status":
                        value = {v: field.name, f: `<span class="status_${field.statusCategory.colorName}">${field.name}</span>`};
                        break;
                    case "labels":
                        value = field.join(" ");
                        value = {v: value, f: value};                        
                        break;
                    case "type":
                    case "priority":
                        value = field ? {v: field.id, f: `<img width="16" src="${field.iconUrl}" title="${field.name}">`} : {v: "", f: ""};
                        break;
                    default:
                        value = field ? field : "";
                        value = {v: value, f: value};
                }
            }
            return value;
        }

        /**
         * Macro for document.getElementById. 
         * @param {*} DOMElementId DOM element to search.
         * @returns THE DOM element if exists.
         */
        function $$(DOMElementId){ return document.getElementById(DOMElementId);}

        /**
         * Generic one line request to Jira.
         * @param {resource} resource url to request.
         */ 
        async function _chtmlm_gft_generic_fetch(resource){
            //let credentials = btoa("user:pass");
            let error;
            let url = `${config['server']}/rest/api/latest${resource}`;
            let auth = { "Authorization" : `Basic ${config['PAT']}`};
            let fetchRes = await fetch(url, {headers : auth})
                                .catch( response_error => { error = response_error;} );
            let response;
            if (fetchRes) { response = await fetchRes.json().catch(  response_error => { error = response_error;} );}
            if (!response) {
                console.error(error.stack);
                response = {};
                response['url'] = url;
                response['errorMessages'] = [];
                response['errors'] = error;
                response['errorMessages'].push(error.message);           
            }
            return response;
        }

        /**
         * Request a paginated JQL query to jira.
         * @param {resource} resource url to request.
         */
        async function _chtmlm_gft_paginated_fetch(resource, resultType){
            let downloadedPages = 0;
            let pageSize = 100;
            let currentPage = 0;
            let totalPages = 0;
            let pages = [];
            
            // Load all pages asynchronously (limit 50 at same time).
            while (currentPage <= totalPages){
                _chtmlm_gft_generic_fetch(resource+`&maxResults=${pageSize}&startAt=${currentPage*pageSize}`).then ( responseJSON => {
                    // If we have error messages show an alert otherwise store response.
                    if ( responseJSON['errorMessages']){ $$('error_bar').innerHTML=responseJSON['errorMessages']; }
                    else {pages[ (responseJSON.startAt / pageSize) ] = responseJSON;}
                    downloadedPages++;	
                })
                
                // Wait for the first page to get info about how many pages to download.
                if (currentPage == 0) {
                    while (downloadedPages == 0){await _sleep();}
                    totalPages = Math.ceil(pages[0].total / pageSize) - 1;
                }

                // If we arrive to the last page, wait until all are downloaded.
                if (currentPage == totalPages) { while (currentPage >= downloadedPages){await _sleep();} }

                // Limit number of downloading pages at the same time to avoid jira throtelling.
                currentPage++;
                while (currentPage - downloadedPages > 50 ){await _sleep();}                
            }

            // Prepare results with all the pages sorted.
            let result = pages[0];
            if (!result[resultType]) {result[resultType] = [];}
            for (let i=1; i<pages.length; i++){
                result[resultType] = result[resultType].concat(pages[i][resultType]);
            }
            return result;
        }
        /**
         * Format a versions array field into printable strings.
         * @param {Array} versions_array 
         * @returns A string with the available versions separated by spaces in the array or an empty string if none exists.
         */
        function _chtmlm_gft_format_versions_array(versions_array)
        {
            let versions = [];
            if (versions_array){
                for (let i=0; i<versions_array.length; i++){
                    versions.push(versions_array[i].name);
                }
            }
            return versions.join(" "); // Don't use comma to avoid problems with csv exports.
        }
        /**
        * No-op sleep to wait until timeout is over
        * @param {integer} timeout milliseconds to wait.
        * @returns A promise that finishes passed the timeout.
        */
        async function _sleep(timeout=null){return new Promise(r => setTimeout(r, timeout));}

        /**
         * 
         */
        function on_google_library_load(){
            google.charts.load('current', {'packages':['table', 'controls', 'corechart']});
            google.charts.setOnLoadCallback(refresh_confluence_haley_component);
        }

        /** Execute on script load. **/
        //const google_library = document.createElement('script');
        //google_library.src = 'https://www.gstatic.com/charts/loader.js';
        //google_library.onload = on_google_library_load;
        //document.body.append(google_library);
        
