//define("_ujgJERARQUIAPRINTER", [], function() {let fn = function(API) { _ujg_gft_print_hierarchy_chart(API); }; return fn;});

/*#################################################*/

let csv_data="#4CAF50	Consejero Delegado	César González-Bueno	GONZALEZBUENOC@BANCSABADELL.COM		&\
#0096c8	Banca Particulares	César González- Bueno (en funciones)	temp@bancsabadell.com 	GONZALEZBUENOC@BANCSABADELL.COM	&\
#0096c8	Banca Empresas y Red	Carlos Ventura	VENTURAC@BANCSABADELL.COM	GONZALEZBUENOC@BANCSABADELL.COM	&\
#0096c8	Banca Corporativa	José Nieto	NIETOJ@BANCOSABADELL.COM	GONZALEZBUENOC@BANCSABADELL.COM	&\
#0096c8	Financiación	Jorge Rodríguez	RODRIGUEZJORGEM@BANCSABADELL.COM	temp@bancsabadell.com	&\
#DDF1FF	Hipotecas	TBD	tbd_hipotecas@bancsabadell.com	RODRIGUEZJORGEM@BANCSABADELL.COM	&\
#DDF1FF	Préstamos Personales	TBD	tbd_prestamos@bancsabadell.com	RODRIGUEZJORGEM@BANCSABADELL.COM	&\
#DDF1FF	Emisión	Anna Puigoriol	PUIGORIOLANNA@BANCSABADELL.COM	RODRIGUEZJORGEM@BANCSABADELL.COM	&\
#DDF1FF	Sabadell Consumer Finance	Miguel Costa	COSTAM@BANCSABADELL.COM	RODRIGUEZJORGEM@BANCSABADELL.COM	&\
#0096c8	Cuentas y Seguros	Manuel Tresánchez	TRESANCHEZ@BANCSABADELL.COM	temp@bancsabadell.com	&\
#DDF1FF	Seguros de Protección	Francisco Galiano	FGALIANO@BANCSABADELL.COM	TRESANCHEZ@BANCSABADELL.COM	&\
#DDF1FF	Banca Seguros	T. Campos	tbd_banca_seguros@bancsabadell.com	TRESANCHEZ@BANCSABADELL.COM	&\
#DDF1FF	Cuentas	David Luque	LUQUED@BANCSABADELL.COM	TRESANCHEZ@BANCSABADELL.COM	&\
#0096c8	Ahorro Inversión	María Concepción Álvarez	ALVAREZMAR@BANCSABADELL.COM	temp@bancsabadell.com	&\
#DDF1FF	Soluciones Ahorro Inversión	Francesc-Xavier Blanquet	BLANQUETFX@BANCSABADELL.COM	ALVAREZMAR@BANCSABADELL.COM	&\
#DDF1FF	Planificación	Ana-Isabel Rubio	RUBIOANA@BANCSABADELL.COM	ALVAREZMAR@BANCSABADELL.COM	&\
#DDF1FF	Segmento Banca Personal	Jaume Ferrer	FERRERJAU@BANCSABADELL.COM	ALVAREZMAR@BANCSABADELL.COM	&\
#0096c8	Marketing	Luís Pons	PONSL@BANCSABADELL.COM	temp@bancsabadell.com	&\
#DDF1FF	Marketing Analytics y Avanzado de Clientes	Pier Paolo Rossi	ROSSIP@BANCSABADELL.COM	PONSL@BANCSABADELL.COM	&\
#DDF1FF	Marketing Operativo y Publicidad	Sonia Rico	RICOS@BANCSABADELL.COM	PONSL@BANCSABADELL.COM	&\
#DDF1FF	Marketing y Producción Digital	Marion Bauer	BAUERC@BANCSABADELL.COM	PONSL@BANCSABADELL.COM	&\
#DDF1FF	Operaciones de Marketing	Anna Tápies	TAPIESANNA@BANCSABADELL.COM	PONSL@BANCSABADELL.COM	&\
#DDF1FF	Experiencia de Cliente	Manuel Sancho	MSANCHO@BANCSABADELL.COM	PONSL@BANCSABADELL.COM	&\
#DDF1FF	Marca	Mónica Martorell	MARTORELLMO@BANCSABADELL.COM	PONSL@BANCSABADELL.COM	&\
#0096c8	Banca Privada	Ramón de la Riva	DELARIVAR@SABADELLURQUIJO.COM	temp@bancsabadell.com	&\
#DDF1FF	Segmento Banca Privada	Lucía Pérez de Arenaza	PEREZALU@BANCSABADELL.COM	DELARIVAR@SABADELLURQUIJO.COM	&\
#DDF1FF	Urquijo Gestión	Jaime Hoyos	HOYOSJAIME@SABADELLURQUIJO.COM	DELARIVAR@SABADELLURQUIJO.COM	&\
#DDF1FF	Oferta Comercial y Productos de Alto Valor	Alfonso García	GARCIAALONSO@SABADELLURQUIJO.COM	DELARIVAR@SABADELLURQUIJO.COM	&\
#DDF1FF	EAFIs y Sports | Entertainment	C. Muñoz	tbd_eafis@bancsabadell.com	DELARIVAR@SABADELLURQUIJO.COM	&\
silver	Asesoría Jurídica de Particulares	Joaquin Roig	ROIGJ@BANCSABADELL.COM	temp@bancsabadell.com	&\
silver	Operaciones y Tecnología Particulares	Pol Navarro	pol@innocells.io	temp@bancsabadell.com	&\
#DDF1FF	Digital	TBD	tbd_digital@innocells.io	pol@innocells.io	&\
#DDF1FF	Diseño y Servicios Digitales	Adrià Batlle	adria@innocells.io	tbd_digital@innocells.io	&\
#DDF1FF	Diseño	Silver Bruna	silver@innocells.io	adria@innocells.io	&\
#DDF1FF	Soluciones	Paricia Lahoz	patri@innocells.io	adria@innocells.io	&\
#DDF1FF	Producto	Sergio Uceda	sergi.uceda@innocells.io	adria@innocells.io	&\
#DDF1FF	Kon-Tiki	Silver Bruna	silver01@innocells.io	adria@innocells.io	&\
#DDF1FF	Engineering	Jaime Muñoz	MUNOZJAI@BANCSABADELL.COM	tbd_digital@innocells.io	&\
#DDF1FF	Partnerships	TBD	tbd_partnerships@bancsabadell.com	tbd_digital@innocells.io	&\
#DDF1FF	Operación y Desarrollo de Producto	Juanma Cid	CID.JUANMANUEL@BANCSABADELL.COM	pol@innocells.io	&\
#DDF1FF	Gestión de Proyectos Digitales	Alba Roig	alba@innocells.io	CID.JUANMANUEL@BANCSABADELL.COM	&\
#DDF1FF	Plataformas Clientes	Marta Amo	AMOM@SABIS.TECH	pol@innocells.io	&\
#DDF1FF	Gestión de Servicios y Planificación	TBD	tbd_gestion_servicios_plani@bancsabadell.com	pol@innocells.io	&\
#DDF1FF	Product Governance	Daniel Gómiz	GOMIZDANIEL@BANCSABADELL.COM	pol@innocells.io	&\
#0096c8	Red	Jaime Matas	MATAS@BANCSABADELL.COM	VENTURAC@BANCSABADELL.COM	&\
#0096c8	Pymes, Negocios y Autónomos	Albert Figueras	FIGUERASAL@BANCSABADELL.COM	VENTURAC@BANCSABADELL.COM	&\
#0096c8	Empresas	Joaquin López	LOPEZFJOAQUIN@BANCSABADELL.COM	VENTURAC@BANCSABADELL.COM	&\
silver	Asesoría Jurídica Empresas y Red	Sílvia Suñé	SUNES@BANCSABADELL.COM	VENTURAC@BANCSABADELL.COM	&\
silver	Operaciones y Tecnología Empresas y Red	Rüdiger Schmidt	SCHMIDTR@BANCSABADELL.COM	VENTURAC@BANCSABADELL.COM	&\
#DDF1FF	Oficinas	Rosa Mª Boronat	BORONATR@BANCSABADELL.COM	SCHMIDTR@BANCSABADELL.COM	&\
#DDF1FF	Digital	Denis Nakagaki	denis@innocells.io	SCHMIDTR@BANCSABADELL.COM	&\
#DDF1FF	Producto/UX	Cristian Raset	RASETC@BANCSABADELL.COM	denis@innocells.io	&\
#DDF1FF	Product Owners	TBD	tbd_POs@bancsabadell.com	RASETC@BANCSABADELL.COM	&\
#DDF1FF	Business Analysts	Beatriz Ciudad	bea.ciudad@innocells.io	RASETC@BANCSABADELL.COM	&\
#DDF1FF	Research and Design	Hernan Lew	hernan@innocells.io	RASETC@BANCSABADELL.COM	&\
#DDF1FF	Desarrollo Digital	Sergio Uceda	sergi.uceda@innocells.io	denis@innocells.io	&\
#DDF1FF	Development	Enrique Nova	enrique@innocells.io	sergi.uceda@innocells.io	&\
#DDF1FF	Tech	David Morón	david.moron@innocells.io	sergi.uceda@innocells.io	&\
#DDF1FF	Proyectos	Joan Font	joan.font@innocells.io	denis@innocells.io	&\
#DDF1FF	PMO	Jordi Petchamé	jordi.petchame@innocells.io	joan.font@innocells.io	&\
#DDF1FF	Project Management	TBD	tbd_pm@bancsabadell.com	joan.font@innocells.io	&\
#DDF1FF	Servicing	Jose Esteve	ESTEVEJO@BANCSABADELL.COM	SCHMIDTR@BANCSABADELL.COM	&\
#DDF1FF	Financiaciones	Jordi Busquets	BUSQUETSJ@BANCSABADELL.COM	SCHMIDTR@BANCSABADELL.COM	&\
#DDF1FF	Banca Transaccional	Antonio Jurado	JURADOPEREZA@BANCSABADELL.COM	SCHMIDTR@BANCSABADELL.COM	&\
#DDF1FF	Control Interno	Gemma Mercadé	MERCADER@BANCSABADELL.COM	SCHMIDTR@BANCSABADELL.COM	&\
#DDF1FF	Service Management	Ana Verdú	AVERDU@BANCSABADELL.COM	SCHMIDTR@BANCSABADELL.COM	&\
silver	Operaciones y Tecnología	Marc Armengol	ARMENGOLM@BANCSABADELL.COM	GONZALEZBUENOC@BANCSABADELL.COM	&\
silver	Tecnología	Carlos Abarca	ABARCA@BANCSABADELL.COM	ARMENGOLM@BANCSABADELL.COM	&\
#DDF1FF	Seguridad Tecnológica	Javier Serrano	SERRANOX@BANCSABADELL.COM	ABARCA@BANCSABADELL.COM	&\
#DDF1FF	Gobernanza Tecnológica	Alberto Prades	PRADES.ALBERTO@BANCSABADELL.COM	ABARCA@BANCSABADELL.COM	&\
#DDF1FF	Arquitectura	TBD	tbd_arquitectura@bancsabadell.com	ABARCA@BANCSABADELL.COM	&\
#DDF1FF	SABIS - Transformación Tecnológica	Gorka Mendizábal	MENDIZABALG@SABADELL.CO.UK	ABARCA@BANCSABADELL.COM	&\
#DDF1FF	SABIS - Metodología y Estándares Tecnológicos	Susana Solver	SSOLER@SABIS.TECH	ABARCA@BANCSABADELL.COM	&\
";

/*#################################################*/

let csv_legend="#4CAF50	CEO		golden@example.com	golden@example.com &\
#0096c8	Direcciones de negocio		dark@example.com	dark@example.com &\
silver	Direcciones de soporte		silver@example.com	silver@example.com &\
";

function _ujg_gft_print_hierarchy_chart(__ujgAPI){
	_ujgAPI = __ujgAPI;
  google.charts.load('current', {packages: ["orgchart"]});
  google.charts.setOnLoadCallback(_ujg_gft_draw_Charts);
}

function _ujg_gft_draw_Charts(){
  _ujg_gft_drawChart(document.getElementById('legend_div'), csv_legend);
  _ujg_gft_drawChart(document.getElementById('chart_div'), csv_data);
  _ujgAPI.resize();
}

function _ujg_gft_drawChart(div_obj_to_draw, csv_to_draw ) {
  var data = new google.visualization.DataTable();
  data.addColumn('string', 'Name');
  data.addColumn('string', 'Manager');
  data.addColumn('string', 'ToolTip');
  data.addColumn('string', 'style');
  data.addRows( data_to_rows(csv_to_draw));

  // Create the chart.
  var chart = new google.visualization.OrgChart(div_obj_to_draw);
  
  // Draw the chart, setting the allowHtml option to true for the tooltips.
  var options = {'allowHtml': true, 'allowCollapse': true };

  // Set row colour if is set
  for (i = 0; i < data.Wf.length; i++) {
    let color = data.Wf[i].c[3].v;
    data.setRowProperty(i, 'style', `background: ${color}; border-color: ${color}; `);
  }

  // Draw the chart.
  chart.draw(data, options);

}

function data_to_rows(csv_rows_data){
  let response = [];
  let table = csv_rows_data.split("&");
  for (i = 0; i < table.length-1; i++) {
    let newrow = [];
    let row = table[i].split("\t");
    let vf_pair = {};
    vf_pair['v'] = row[3].trim().toLowerCase();
    if (vf_pair['v'].indexOf('@example.com') > 0){ // non linkable domain.
      vf_pair['f'] = `${row[1].trim()}`;
    }else{
      vf_pair['f'] = `${row[1].trim()}<br><a href='mailto:${vf_pair['v']}'>${row[2].trim()}</a>`;
    }    
    newrow.push(vf_pair);
    newrow.push(row[4].trim().toLowerCase());
    newrow.push(row[1].trim());
    newrow.push(row[0].trim());
    response.push(newrow);
    }
  return response;
  }

function _ujg_gft_zoomIn(elementToZoomId)
{
  elementToZoom = document.getElementById(elementToZoomId);
  elementToZoom.style.zoom = parseFloat(elementToZoom.style.zoom)  + 0.10;
}

function _ujg_gft_zoomOut(elementToZoomId)
{
  elementToZoom = document.getElementById(elementToZoomId);
  elementToZoom.style.zoom = parseFloat(elementToZoom.style.zoom)  -0.10;
}

function _ujg_gft_zoomReset(elementToZoomId)
{
  elementToZoom = document.getElementById(elementToZoomId);
  elementToZoom.style.zoom = 0.5;
}