//define("_ujgREPORTSQUADS", [], function() {let fn = function(API) { _ujg_gft_report_squads_main(API); }; return fn;});
server = "https://innocells.atlassian.net";
apiJQL = server + "/rest/api/latest/search?jql=";

function _ujg_gft_generic_onError(data){
	console.debug(data);
	alert('Se ha producido un error al cargar los datos.\n' + JSON.parse(data).errorMessages);
	_ujgAPI.resize();
}

function _ujg_gft_report_squads_main(__ujgAPI){
	_ujgAPI = __ujgAPI;

    // Global chart.
    JQL = `project = BSD AND issueType=story AND ( (status = done AND sprint in openSprints ())  OR status in (BACKLOG, DOR, "TO DO", DOING, "QA REVIEW", "UI REVIEW", "PO REVIEW", BLOCKED, DOD) ) AND "Epic Link" is not empty`;
    _ujg_gft_report_squads_load_JQL_and_draw(JQL, 'status', 'epic_chart_global','us_chart_global' );

    // Squads charts.
    let squads =[
        ['SQUAD_5-STAR','5-star - Particulares'],
        ['SQUAD_ASISTENCIA_DIGITAL','Asistencia digital - Asistencia digital'],
        ['SQUAD_AWARENESS','Awareness - Awareness'],
        ['SQUAD_ACQUISITION','Cliente digital - Acquisition'],
        ['SQUAD_MAINTENANCE','Cliente digital - Mantenimiento'],
        ['SQUAD_FINANCIACION','Financiación - Financiación'],
        ['SQUAD_TARJETAS_SERVICING','Tarjetas - Servicing'],
        ['SQUAD_TARJETAS_VENTAS','Tarjetas - Ventas'],
        ['SQUAD_TRANSFERENCIAS','Transferencias - Transferencias']
    ];

    // Iterate through different squads.
    let container;
    let row;
    
    for (let i=0; i < squads.length; i = i + 1)
    {
        let cell;
        if(i % 2 == 0){ // even column (base 0)
            container = document.createElement ('TABLE');
            document.getElementById('squads_output').appendChild(container);
            row = container.insertRow(0);
            cell = row.insertCell(0);
        }else{ //odd column
            cell = row.insertCell(1);
        }

        let newItem;
        newItem = document.createElement ('TABLE');
        newItem.innerHTML = document.getElementById('table_template').innerHTML;
        let squadId = encodeURIComponent([squads[i][1]]);
        
        newItem.id = `${newItem.id}${squadId}`;
        newItem.style="visibility: visible ;";
        newItem.getElementsByTagName("h3")[0].innerText = "Squad: " + squads[i][1];
        
        divs = newItem.getElementsByTagName("div");
        for (let j=0; j < divs.length ; j++ ){
            divs[j].id = `${divs[j].id}${squadId}`;
        }

        JQL = `project = BSD AND issueType=story AND (Labels in (${squads[i][0]}) OR cf[10081] = "${squads[i][1]}") AND ( (status = done AND sprint in openSprints ())  OR status in (BACKLOG, DOR, "TO DO", DOING, "QA REVIEW", "UI REVIEW", "PO REVIEW", BLOCKED, DOD) ) AND "Epic Link" is not empty`;
        _ujg_gft_report_squads_load_JQL_and_draw(JQL, 'status', divs[0].id,divs[1].id );

        cell.appendChild(newItem);

    }
}

async function _ujg_gft_report_squads_load_JQL_and_draw(JQL, keyField, epicChartId, usChartId){
    let queryURL = apiJQL + JQL;
    let responseJSON = "";
    
    // Initialize response data.
    let epics = Object.create( {} );
    epics.issues = [];
    
    let user_stories = Object.create( {} );
    user_stories.issues = Object.create( {} );
    user_stories.issues.length = 0;
    user_stories.total = 1; // set to 1 to enter the bucle.
    user_stories.maxResults = 10000; // Set as higher possible value (it will be corrected by API limits.)

    // Load all issues of this JQL.    
    while (user_stories.issues.length<user_stories.total)
    {
        pURL = queryURL+`&maxResults=${user_stories.maxResults}&startAt=${user_stories.issues.length}`;
        responseJSON = "";
        AP.request({
            url: pURL ,
            dataType: "json",
            success : function(server_response) {
                responseJSON = JSON.parse(server_response); },
            error :   function(server_response) {
                responseJSON = JSON.parse(server_response); _ujg_gft_generic_onError(server_response);
            }
        });
        
        while (responseJSON == ""){await new Promise(r => setTimeout(r, 1000));}

        if (responseJSON.errorMessages){
            break;
        }else{
            if (user_stories.issues.length == 0){
                user_stories = responseJSON;
            }else{
                user_stories.issues = user_stories.issues.concat(responseJSON.issues);
            }
        }
    }
    
    // Load all related epics.
    let epics_search = [];
    epics_search[1] = {}; // First level epics.
    epics_search[2] = {}; // Second level epics. (need to research twice)

    // Explore all user_stories searching related epics.
    for (let i = 0; i < user_stories.issues.length; i++)
    {
        let issue = user_stories.issues[i];

        if (issue.fields.customfield_10008.split("-")[0] == "BSDROAD"){
            epics_search[1][issue.fields.customfield_10008] = issue.fields.customfield_10008;
        }else{
            epics_search[2][issue.fields.customfield_10008] = issue.fields.customfield_10008;
        }
    }

    // Search all second level epics to find first level ones.
    if (Object.keys(epics_search[2]).length > 0)
    {
        responseJSON = "";
        AP.request({
            url: apiJQL + `issueKey in (${Object.keys(epics_search[2]).toString()})`  ,
            dataType: "json",
            success : function(server_response) {responseJSON = JSON.parse(server_response); },
            error :   function(server_response) {responseJSON = JSON.parse(server_response); _ujg_gft_generic_onError(server_response);}
        });

        while (responseJSON == ""){
            await new Promise(r => setTimeout(r, 1000));
        }
        
        // Sanitize summary and try to extract BSDROAD epic name.
        for (let i = 0; i < responseJSON.total; i++)
        {
            let issue = responseJSON.issues[i];
            let toSearch = issue.fields.customfield_10005 + issue.fields.summary;
            toSearch = toSearch.replace("BRIOPRD","BSDROAD");
            toSearch = toSearch.replace("[","");
            toSearch = toSearch.replace("]"," ");
            toSearch = toSearch.replace("/"," ");
            toSearch = toSearch.replace("|"," ");
            let parent = toSearch.split("BSDROAD-");

            if ( parent.length > 1 ){
                // If we found a BSDROAD, we put it to search.
                let epic = "BSDROAD-" + parent[1].split(" ")[0];
                epics_search[1][epic]=epic;
            }else{
                // If no BSDROAD info is found, add it directly.
                console.info(`No se ha podido deducir parent de ${issue.key}: ${issue.fields.customfield_10005}`);
                epics_search[1][issue.key]=issue.key;
            }
        }
    }
    
    // Search all first level epics.
    let epic_JQL =  `issueKey in (${Object.keys(epics_search[1]).toString()})`;

    if (Object.keys(epics_search[1]).length > 0){

        responseJSON = "";
        AP.request({
            url: apiJQL + epic_JQL  ,
            dataType: "json",
            success : function(server_response) {responseJSON = JSON.parse(server_response); },
            error :   function(server_response) {responseJSON = JSON.parse(server_response); _ujg_gft_generic_onError(server_response);}
        });

        while (responseJSON == ""){
            await new Promise(r => setTimeout(r, 1000));
        }
        epics = responseJSON;
            
    }

    _ujg_gft_drawPieChart(epicChartId, epics, keyField, epic_JQL); 
    _ujg_gft_drawPieChart(usChartId, user_stories, keyField, JQL);
    _ujgAPI.resize();
}

function _ujg_gft_drawPieChart(chartDiv, data, keyField, JQLLink)
{
    // Group by using keyField as agrupator.
    let lookUpTable = {};
    let lookUp = 1;
    let count = [];   
    count[0] = [keyField,"value"]; 
    for (i = 0; i < data.issues.length; i++)
    {
        value = data.issues[i].fields[keyField].name;
        if (lookUpTable[value]){
            lookUp = lookUpTable[value] ; 
        }else{
            lookUp = Object.keys(lookUpTable).length + 1;
            lookUpTable[value] = lookUp;
            count[lookUp] = [value, 0];
        }
        count[lookUp][1]++;
    }

    // Convert dictionary to google needed array data.
    _ujg_gft_drawChart('', chartDiv, count, JQLLink);
}

function _ujg_gft_drawChart(charTitle, elementId, arrayData, JQLLink)
{
    var data = google.visualization.arrayToDataTable(arrayData);

    let options = {
        title: charTitle,
        is3D: false,
        pieSliceText: 'value',
        chartArea:{left:10,top:10,width:'95%',height:'95%'},
        legend: {position: 'labeled'},
        tooltip:{ trigger:'none'}
        };

    var chart = new google.visualization.PieChart(document.getElementById(elementId));
    chart.JQLLink = JQLLink;

    var clickHandler = function (e){
        if (chart.getSelection() != null && chart.getSelection().length > 0 && chart.getSelection()[0].row != null ){
            chart.lastSelection = chart.getSelection();
        }
        if (chart.lastSelection != null) {
            let status = data.getValue(chart.lastSelection[0].row, 0);
            let url = server + '/issues/?jql='+chart.JQLLink+' AND status="'+status+'"';
            window.open(url, '_blank');  
        }
    };

    google.visualization.events.addListener(chart, 'select', clickHandler);
    chart.draw(data, options);
}

google.charts.load('current', {'packages':['corechart']});
        