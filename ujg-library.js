function insertIssue(objTabla, issue)
{
	var fields = issue.fields;
	var row = objTabla.insertRow(-1);
	insertCell(row, fields.assignee.displayName);
	insertCell(row, fields.issuetype.name);
	insertCell(row, fields.customfield_10046);
	insertCell(row, fields.customfield_10248);
	insertCell(row, fields.status.name);
	insertCell(row, fields.customfield_10109.value);
}

function insertCell(row, value)
{
	var cell = row.insertCell(-1);
	cell.innerHTML = value;
}