//define("_ujgDEPMAN", [], function() {let fn = function(API) { _ujg_gft_depman_main(API); }; return fn;});
let _ujgAPI = null;
server = "https://innocells.atlassian.net";
browse = server +"/browse/";
apiJQL = server + "/rest/api/latest/search?jql=";
apiREST = server + "/rest/api/latest";
url_teams = apiREST + "/issue/createmeta?projectKeys=DEPMAN&expand=projects.issuetypes.fields";
url_epics = apiJQL + "project='DEPMAN' AND status NOT IN (closed) AND cf[10249] in ('${team}') ORDER BY RAG";
linkType = 'outwardIssue';
team_colours = ["#EBEAA7", "#FFF7AC", "#FACF9C", "#F3A688", "#F4AFCC", "#E8A8C2", "#CCAED0", "#BA93C0", "#B1A6CE", "#9BBFE3", "#AEDEF4", "#B6DEDE", "#EBEAA7"];

function _ujg_gft_depman_onError(data)
{
	console.debug(data);
	alert('Se ha producido un error al cargar los datos.');
	_ujgAPI.resize();
}

function _ujg_gft_depman_main(__ujgAPI)
{
	_ujgAPI = __ujgAPI;
	AP.request({
		url: url_teams,
		dataType: "json",
		success : function(data) {_ujg_gft_depman_printTeams(JSON.parse(data));},
		error :   function(data) {_ujg_gft_depman_onError(data);}
	});
}

function _ujg_gft_depman_printTeams(metadata)
{
	let visible  = document.getElementById('visible');
	let data = metadata.projects[0].issuetypes[0].fields.customfield_10249.allowedValues;

	for (let key = 0; key < data.length; key++) {
		let title = document.getElementById('_ujg_gft_depman_title_template').cloneNode(true);
		let team = data[key].value;

		title.id=title.id+"_"+key;
		title.innerHTML = data[key].value;
		title.style.backgroundColor = team_colours[key];

		let table = document.getElementById('_ujg_gft_depman_main_table').cloneNode(true);
		table.id=table.id+"_"+data[key].id;

		visible.appendChild(title);
		visible.appendChild(table);

		AP.request({
			url: eval('`'+url_epics+'`'),
			dataType: "json",
			success : function(data) {_ujg_gft_depman_printEpics(JSON.parse(data), table);},
			error :   function(data) {_ujg_gft_depman_onError(data);}
		});

	}
}

function _ujg_gft_depman_printEpics(data, objTabla)
{
	if (data.total > 0){
		let pijama = "_ujg_gft_depman_pijama_B"; // Set to B to start with A.
		for (let key = 0; key < data.total; key++) {
			if ( data.issues[key].fields.issuelinks.length>0 ){
				let validType = false;
				for (let key2 = 0; key2 < data.issues[key].fields.issuelinks.length; key2++) {
					validType = validType | (eval(`data.issues[key].fields.issuelinks[key2].${linkType}`) != null);
				}
				if (validType){
					if (pijama=="_ujg_gft_depman_pijama_B"){pijama = "_ujg_gft_depman_pijama_A";}
					else {pijama = "_ujg_gft_depman_pijama_B";}
					_ujg_gft_depman_printEpic(objTabla, data.issues[key], pijama);
				}
			}
		}
	}else{
		objTabla.remove();
	}
	_ujgAPI.resize();
}

function _ujg_gft_depman_printEpic(objTabla, issue, pijamaClass="")
{
	
	let fields = issue.fields;
	let row = objTabla.insertRow(-1);
	row.classList.add("_ujg_gft_depman_table_data");
	if (pijamaClass!="") {row.classList.add(pijamaClass);}

	/* Issue Key */
	_ujg_gft_library_insertCellLinkable(row, `${issue.key}`, browse+issue.key, `: ${issue.fields.summary}`);

	/* Responsible */
	try {_ujg_gft_library_insertCellText(row, fields.customfield_10265);}
	catch (error) {_ujg_gft_library_insertCellText(row, "No assignada");}
	
	/* Progress */
	try {_ujg_gft_library_insertCellText(row, fields.customfield_10248+"%");}
	catch (error) {_ujg_gft_library_insertCellText(row, "--");}

	/* Fechas */
	try {
		let fechas =  fields.customfield_10046;
		let fechaIni = new Date( fields.customfield_10045);
		let fechaFin = new Date( fields.customfield_10046);
		
		_ujg_gft_library_insertCellText(row, _ujg_gft_shortDate(fechaIni)+" "+_ujg_gft_shortDate(fechaFin));
	}
	catch (error) {_ujg_gft_library_insertCellText(row, "");}

	/* Status */
	_ujg_gft_library_insertCellImage(row, fields.status.iconUrl, fields.status.name);

	/* RAG */
	try {
		icon = _ujg_gft_library_RAGTextToSVG(fields.customfield_10109.value);
		_ujg_gft_library_insertCellImage(row, iconUrl=icon, fields.customfield_10109.value, 8);
	}
	catch (error) {_ujg_gft_library_insertCellText(row, "--");}

	/* Print sub-dependencies in nested table */
	_ujg_gft_depman_printDependecies (row.insertCell(-1), issue);
}

function _ujg_gft_depman_printDependecies(cell, issue)
{
	// Preselect all relatedLinks.
	let related = [];
	let links = issue.fields.issuelinks;
	for (let key = 0; key < links.length; key++) {
		if (links[key][linkType]) {
			related.push('"'+links[key][linkType].key+'"');
		}
	}

	// Only show table if related issues exists.
	if (related.length>0)
	{
		// Copy _ujg_gft_subtable_header structure to use as template.
		let subTable = document.createElement('table');
		subTable.innerHTML = document.getElementById('_ujg_gft_subtable_header').innerHTML;
		subTable.deleteTHead();
		subTable.style.width="100%";

		// Add as new nested table.
		cell.append(subTable);

		// Send the request to print.
		AP.request({
				url: apiJQL + 'issuekey in ('+related.join(',')+') ORDER BY RAG',
				dataType: "json",
				success : function(data) {_ujg_gft_depman_printDependeciesList(subTable , JSON.parse(data));},
				error :   function(data) {_ujg_gft_depman_onError(data);}
				});
	}
}

function _ujg_gft_depman_printDependeciesList(subTable, data)
{
	let issues = data.issues;

	for (let key = 0; key < issues.length; key++) {
		let row = subTable.insertRow(-1);
		row.classList.add("_ujg_gft_depman_table_data");
		issue  = issues[key];
		fields = issue.fields;

		/* Priority */
		try {_ujg_gft_library_insertCellImage(row, fields.priority.iconUrl, fields.priority.name, 12);}
		catch (error) {_ujg_gft_library_insertCellText(row, "");}

		/* Issue Key & Summary*/
		_ujg_gft_library_insertCellLinkable(row, `${issue.key}`, browse+issue.key, `: ${issue.fields.summary}`);

		/* RAG */
		try {
			icon = _ujg_gft_library_RAGTextToSVG(fields.customfield_10109.value);
			_ujg_gft_library_insertCellImage(row, iconUrl=icon, fields.customfield_10109.value, 8);
		}
		catch (error) {_ujg_gft_library_insertCellText(row, "-");}

		/* SM/PM */
		try {_ujg_gft_library_insertCellText(row, fields.customfield_10063.displayName);}
		catch (error) {_ujg_gft_library_insertCellText(row, "---");}

		/* Releases */
		try {
			let versions = [];
			for (let key2 = 0; key2 < issue.fields.fixVersions.length; key2++){
				versions.push(issue.fields.fixVersions[key2].name);
			}
			_ujg_gft_library_insertCellText(row, versions.join(", "));	
		}
		catch (error) {_ujg_gft_library_insertCellText(row, "---");}
	}
}

function _ujg_gft_library_insertCellText(row, value)
{
	let cell = row.insertCell(-1);
	cell.innerHTML = value;
}

function _ujg_gft_library_insertCellImage(row, imageURL, tooltip='', width='')
{
	let cell = row.insertCell(-1);
	cell.innerHTML = `<img alt="${tooltip}" title="${tooltip}" width="${width}" src="${imageURL}">`;
}

function _ujg_gft_library_insertCellLinkable(row, value, link, postText = null)
{
	let cell = row.insertCell(-1);

	if (!postText){
		cell.innerHTML = `<a target="_blank" href="${link}">${value}</a>`;
	}else{
		cell.innerHTML = `<a target="_blank" href="${link}">${value}</a>${postText}`;
	}

}

function _ujg_gft_library_insertHeaders(table, headersArray)
{
	let header=  document.createElement('thead');
    let headingRow = document.createElement('tr');

	for (let key = 0; key < headersArray.length; key++) {
		_ujg_gft_library_insertHeader(headingRow, headersArray[key]);	
	}
	header.appendChild(headingRow);
    table.appendChild(header);
}

function _ujg_gft_library_insertHeader(row, text)
{
	let headingCell = document.createElement('th');
    let headingText = document.createTextNode(text);
    headingCell.appendChild(headingText);
    row.appendChild(headingCell);
}

function _ujg_gft_library_RAGTextToSVG(RAG)
{   
	switch (RAG.charAt(0).toUpperCase())
	{
		case "R":  return "https://upload.wikimedia.org/wikipedia/commons/9/92/Location_dot_red.svg";
		case "A":  return "https://upload.wikimedia.org/wikipedia/commons/5/59/Location_dot_yellow.svg";
		case "G": return "https://upload.wikimedia.org/wikipedia/commons/0/0e/Location_dot_green.svg";
		default: return RAG; // If we are here, is a not expected behaviour.
	}
}

function _ujg_gft_shortDate(d)
{
	let ye = new Intl.DateTimeFormat('en', { year: '2-digit' }).format(d);
	let mo = new Intl.DateTimeFormat('en', { month: '2-digit' }).format(d);
	let da = new Intl.DateTimeFormat('en', { day: '2-digit' }).format(d);
	return `${da}/${mo}/${ye}`;
}