/**
 * Function to run on document load.
 */
define("_ujg_gft_collector_generic_onbodyload", [], function() {let fn = function(API) { 
    _ujg_gft_collector_generic_load_project_customfield_selects(document.getElementsByTagName('form')[0]);
     API.resize();
}; return fn;});

/**
 * Function macro for document.getElementById. Use double $ to avoid collision with angular.
*/
function $$ (elementId){ return document.getElementById(elementId); }

/**
 * Fill select custom fields in the form with the right values set on the project schema in JIRA.
 * @param {object} formObject form with selects to fill
 */
function _ujg_gft_collector_generic_load_project_customfield_selects(formObject){
    // Autodiscovery project and issue type
    let projectId = $$('project#key').value;
    let issueType = 'issuetype#id' in formObject.elements ? `issuetypeids=${$$('issuetype#id').value}` :  `issuetypeNames=${$$('issuetype#name').value}`;
   
    // Recovery metadata from jira.
    AP.request({
        url: `/rest/api/latest/issue/createmeta?expand=projects.issuetypes.fields&projectKeys=${projectId}&${issueType}`  ,
        dataType: "json",
        type: "GET",
        contentType: 'application/json',
        success : function(server_response) {
            // Search fields with data-autofill to true, and fill with values in the project schema.
            let fields = JSON.parse(server_response).projects[0].issuetypes[0].fields;
            for (let i=0; i<formObject.length; i++){
                let formElement = formObject[i];
                if (formElement.id){
                    if (formElement.type.substr(0,7) == "select-") {
                        if (formElement.getAttribute('data-autofill') == "true" ){
                            let allowed = fields[formElement.id].allowedValues;
                            for (let j=0; j<allowed.length; j++){
                                let option = document.createElement('option');
                                option.value = allowed[j].id;
                                option.text = allowed[j].value;
                                formElement.add(option);
                            }    
                        }
                    }
                }
            }
        },
        error :   function(server_response) {
            // Something was wrong, log information and show error message.
            console.log(server_response);
            AJS.flag({
                type: 'error',
                close: 'manual',
                body: `Se ha producido un error al cargar la informacion del proyecto.`,
            });
        },
    });
}

/**
 * Convert information in a form into a JSON able to be read as a Jira issue creation post
 * @param {form} formObject form to send to jira.
 * @param {button} sendButtonId form send button. It allows the function to handle (enabe/disable) the button.
 */
function _ujg_gft_collector_generic_submit(formObject, sendButtonId=null)
{
    if (sendButtonId){
        sendButton = document.getElementById(sendButtonId);
        sendButton.disabled = true;
    }
    let newIssue = {};
    newIssue.fields = {};

    // Recovery all id's from form and store as data to the API.
    for (let i=0; i<formObject.length; i++){
        let formElement = formObject[i];
        if (formElement.id){
            // 'ignoreme-' id's are ignored it is reserved for frontend versatility.
            if (formElement.id.substr(0,9) != "ignoreme-" && formElement.type != 'submit'){

                // Get and format value from distinct places depnding on the type.
                let value;

                // Handling field values depending on its type
                switch(formElement.id){
                    case 'labels':
                        value = [];
                        if (document.getElementById('ignoreme-collector_label')){ // Add collector label if exists.
                            value.push(document.getElementById('ignoreme-collector_label').value);
                        }
                        switch (formElement.type){
                            case 'select-one':
                            case 'select-multiple':
                                for (let j=0; j<formElement.selectedOptions.length; j++){
                                    value.push(formElement.selectedOptions[j].value);
                                }
                            break;
                            default:
                                value.push(formElement.value);
                        }
                        break;
                    case 'issuelinks': // Handling special issuelinks
                        if (!newIssue.update){newIssue.update = {};}
                        let issuelink = formElement.value.split("|");
                        newIssue.update.issuelinks = [];
                        let newLink = {};
                        newLink.add = {};
                        newLink.add.type = {};
                        newLink.add.type.name = issuelink[0];
                        newLink.add[issuelink[1]] = {};
                        newLink.add[issuelink[1]].key = issuelink[2];
                        newIssue.update.issuelinks.push(newLink);
                        break;
                    default: // General fields handling.
                        switch (formElement.type){
                            case 'select-one':
                                value = {};
                                value.id = formElement.value;
                                break;
                            case 'select-multiple':
                                value = [];
                                for (let j=0; j<formElement.selectedOptions.length; j++){
                                    let pair = {};
                                    pair.id = formElement.selectedOptions[j].value;
                                    value.push(pair);
                                }
                                break;
                            default:
                                value = formElement.value;
                        }
                }
               
                // Treat as subchild values if a # is present in the label.
                let keyChild = formElement.id.split("#", 2);
                if (keyChild.length==1){
                    newIssue.fields[keyChild[0]] = value;
                }else{
                    let child = {};
                    child[keyChild[1]] = value;
                    newIssue.fields[keyChild[0]] = child;
                }
            }
        }
    }

    // Send the request to JIRA API.
    AP.request({
        url: '/rest/api/latest/issue/' ,
        dataType: "json",
        type: "POST",
        data: JSON.stringify(newIssue),
        contentType: 'application/json',
        success : function(server_response) {
            if (sendButton) {sendButton.disabled = false;}
            formObject.reset();
            issueKey = JSON.parse(server_response).key;
            linkServer = JSON.parse(server_response).self.split("/")[2];
            // Show user information.
            AJS.flag({
                type: 'success',
                close: 'manual',
                body: `Su incidencia <strong>${issueKey}</strong> ha sido creada con éxito. Abra la incidencia si quiere completar la información o <b>añadir adjuntos</b>` +
                    `<ul class="aui-nav-actions-list">` +
                    `<li><a class="aui-button aui-button-link" target="_blank" href="https://${linkServer}/browse/${issueKey}">Abrir ${issueKey}</a></li>`+
                    `</ul>`
            });
        },
        error :   function(server_response) {
            let extra_info;
            console.log(server_response);
            if (sendButton) {sendButton.disabled = false;}

            try{
                extra_info = JSON.stringify(JSON.parse(server_response.responseText).errors);
            }catch(e){
                extra_info = server_response.responseText;
            }
            AJS.flag({
                type: 'error',
                close: 'manual',
                body: `Se ha producido un error al crear la incidencia:<br>${extra_info}`,
            });
        },
    });
}